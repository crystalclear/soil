function syncClick(exerciseRef, DOMposition, audioFilename) {


    var mySignature = "syncClick";

    // Define Global Opacity of Sync Audio
    var syncAudio_Opacity = 1;


    //private constructor
    var __construct = function (that) {

        var xtraHTML = spa.checkExistenceOfXtraHTML(exerciseRef.activity);
        if (xtraHTML["before"] != "")
            DOMposition.appendChild(xtraHTML["before"]);



        var newDiv = document.createElement("DIV");


        /* TITAN NEW FEATURE - Full Width or Defined Horizontal Margin */
        if (exerciseRef.settings.fullWidth) {
            newDiv.className = "activity_content_div col-xs-12";
        } else {
            newDiv.className = "activity_content_div col-xs-10 col-xs-offset-1";
        }

        newDiv.innerHTML += prepareSyncAudioContent(exerciseRef);

        /* newDiv.innerHTML += '<div class="interactive"><div class="boxes"><i class="fa fa-square-o" aria-hidden="true"></i></div><div class="boxes"><i class="fa fa-square-o" aria-hidden="true"></i></div><div class="boxes"><i class="fa fa-square-o" aria-hidden="true"></i></div> </div>' */

        DOMposition.appendChild(newDiv);


        if (xtraHTML["after"] != "")
            DOMposition.appendChild(xtraHTML["after"]);


        /* >> NEW PART - AUDIOPLAYER */
        var dataJSON = exerciseRef.activity.timelineObject;

        /* >> NEW PART II - GROUPS */
        var myGroupIndex = 0;


        // Load HTML Elements display.
        dataJSON.forEach(function (sprite, index) {

            //************************* Howler.js >            
            var contentDiv = document.createElement('div');
            contentDiv.id = 'timelineDiv' + (index + 1);
            contentDiv.className = 'timeline-sprite animated';
            // UNUSED
            //contentDiv.style.opacity = 0;

            contentDiv.innerHTML = sprite.text;
            //************************* Howler.js < 



            //************************* POP Corn >
            contentDiv.style.color = "#565549";

            //Add Custom Class for Images
            /*(sprite.itemClassName || sprite.itemClassName!= undefined) ? += " timelineDivClass " + sprite.itemClassName : " timelineDivClass";*/
            if (sprite.itemClassName != undefined) {
                
                // Create Group Parent
                if (sprite.itemClassName.indexOf('groupParent') != -1) {
                    contentDiv.className += " timelineDivClass " + sprite.itemClassName + " groups" + myGroupIndex;
                    myGroupIndex++;

                } else if (sprite.itemClassName.indexOf('groupChild') != -1) {
                    contentDiv.className += " timelineDivClass " + sprite.itemClassName + " groups" + (myGroupIndex-1);
                } else {
                    contentDiv.className += " timelineDivClass " + sprite.itemClassName;
                }
                

                // Hide Elements that needs to be hidden (such as Images)
                if (sprite.itemClassName.indexOf('opacity_0') != -1) {
                    contentDiv.style.opacity = '0';
                } else {
                    // Apply ACTIVITY's Opacity Defined in JSON and not the Javascript's GLOBAL
                    if (exerciseRef.settings.individualOpacity != undefined) {
                        syncAudio_Opacity = exerciseRef.settings.individualOpacity;
                    }

                    contentDiv.style.opacity = syncAudio_Opacity;
                }
            // IF no other rule applies, just insert Default Class for Timeline / Sync
            } else {
                contentDiv.className += " timelineDivClass";
            }





            sprite.classTitle = sprite.classTitle ? sprite.classTitle : "";
            sprite.title = sprite.title ? sprite.title : "";
            sprite.classText = sprite.classText ? sprite.classText : "";
            sprite.text = sprite.text ? sprite.text : "";

            /* UNUSED
            sprite.innerHTML = sprite.innerHTML ? sprite.innerHTML : "";
            sprite.direction = sprite.direction ? sprite.direction : "";
            */

            var target = document.getElementById(sprite.target),
                container,
                goingUp = true;

            if (target && !target.firstChild) {
                target.appendChild(container = document.createElement("div"));
                container.style.width = "inherit";
                container.style.height = "inherit";
                container.style.overflow = "auto";
            } else if(sprite.itemClassName != undefined && sprite.itemClassName.indexOf('groupChild') != -1){
                    
                    
                var myCurrentGroupIndex = myGroupIndex-1;
                container = target.firstChild.childNodes[myCurrentGroupIndex];

            } else {
                container = target.firstChild;
            }

            //  Default to up if sprite.direction is non-existant or not up or down
            //sprite.direction = sprite.direction || "up";
            sprite.direction = sprite.direction || "down";
            if (sprite.direction.toLowerCase() === "down") {
                goingUp = false;
            }

            if (target && container) {

                
                // if this isnt the first div added to the target div
                if (goingUp) {
                    // insert the current div before the previous div inserted
                    container.insertBefore(contentDiv, container.firstChild);
                } else {
                    container.appendChild(contentDiv);
                }
            }
            
            

            //  Default to empty if not used
            contentDiv.innerHTML = "<p><span class='timelineTitle " + sprite.classTitle + "'>" + sprite.title + "</span>" + "<span class='timelineText " + sprite.classText + "'>" + sprite.text + "</span></p>";

            //************************* POP Corn < 


            /* $("#box1").click(function(){
                alert("The paragraph was clicked.");
            });  */
           /*  $("#box1").click(function(){
                if ($(this).find($(".fa")).hasClass( "fa-check-square-o" )){
                    // $("#popBox1").fadeOut("slow");
                    document.getElementById("#popBox1").style.opacity = 1;
                    alert("ok")
                }
            }); 

            $("#box2").click(function(){
                if ($(this).find($(".fa")).hasClass( "fa-check-square-o" )){
                    // $("#popBox2").fadeOut("slow");
                    document.getElementById("#popBox2").style.opacity = 1;
                }
            });
            $("#box3").click(function(){
                if ($(this).find($(".fa")).hasClass( "fa-check-square-o" )){
                    // $("#popBox3").fadeOut("slow");
                    document.getElementById("#popBox3").style.opacity = 1;
                }
            }); */
            
            $(".boxes").click(function(){
                var currentClick = "#popBox"+($(".boxes").index(this)+1);
                
                if ($(this).find($(".fa")).hasClass( "fa-square-o" )){
                    $(this).find($(".fa")).removeClass('fa-square-o').addClass('fa-check-square-o');
                    //$(currentClick).css('opacity', 1);
                } else {
                    $(this).find($(".fa")).removeClass('fa-check-square-o').addClass('fa-square-o');
                    $(currentClick).css('opacity', 0);
                }
                $( "#checkAnswers" ).fadeIn( "slow", function() {
                    // Animation complete
                  });
            }); 
        });

        /* NEW PART - AUDIOPLAYER << */
        var allSprites = document.querySelectorAll('.timeline-sprite');

        function resetFromAudioPlayer(){
            /* var allClickBoxes = document.querySelectorAll('.boxes');
            var allPopUpBoxes = document.querySelectorAll('.popBoxes'); */
            $(".boxes").find($(".fa")).removeClass('fa-check-square-o').addClass('fa-square-o');
            $(".popBoxes").css('opacity', 0);
            $("#checkAnswers").css('display', "none");
            
        }

        window.parent.loadAudioPlayer(exerciseRef, allSprites, audioFilename, syncAudio_Opacity, resetFromAudioPlayer);
        /* NEW PART - AUDIOPLAYER << */

    } /* End - __construct */





    var prepareSyncAudioContent = function (exerciseRef) {

        var activityPartsHTML = "";
        activityPartsHTML += processParts(exerciseRef.activity.header);
        activityPartsHTML += processParts(exerciseRef.activity.main);
        activityPartsHTML += processParts(exerciseRef.activity.footer);

        return activityPartsHTML;
    };

    var createActText = function (classes, text) {
        var theHTML = '<div class="' + classes + '">';
        var theTexts = text.split("@@");
        theHTML += '<ul>';
        for (var i = 0; i < theTexts.length; i++) {
            theHTML += '<li>' + theTexts[i] + '</li>';
        }
        theHTML += '</ul>';
        theHTML += '</div>';
        return theHTML;
    }

    var processParts = function (parts_array) {
        var theHTML = "";

        // Handle Layout - Handles Different Text specified in timelineObject.target settings [text1/text2 etc]
        var numOfCols = 0;
        for (var currColCounter = 0; currColCounter < parts_array.length; currColCounter++) {
            if ((parts_array[currColCounter] != '') && (parts_array[currColCounter] != null)) {
                numOfCols += 1
            }
        }

        var leftWidth = 0;
        var centerWidth = 12;
        var rightWidth = 0;
        if (numOfCols == 2) {
            if (exerciseRef.settings.equalColumns) {
                leftWidth = rightWidth = centerWidth = 6;
            } else {
                leftWidth = rightWidth = 3;
                centerWidth = 9;
            }
        }
        if (numOfCols == 3) {
            if (exerciseRef.settings.equalColumns) {
                leftWidth = rightWidth = centerWidth = 4;
            } else {
                leftWidth = rightWidth = 3;
                centerWidth = 6;
            }
        }


        var tempHTML = '';
        if ((parts_array[0] != '') && (parts_array[0] != null)) {
            if ($.isArray(parts_array[0][0])) {
                for (var i = 0; i < parts_array[0].length; i++) {
                    if (parts_array[0][i][0].indexOf("syncText") >= 0) {
                        tempHTML += '<div id="' + parts_array[0][i][1] + '"></div>'
                    } else {
                        tempHTML += '<div class="' + parts_array[0][i][0] + '">' + parts_array[0][i][1] + '</div>'
                    }
                }
            } else {
                tempHTML = '<div class="' + parts_array[0][0] + '">' + parts_array[0][1] + '</div>';
            }
            theHTML += '<div class="col-md-' + leftWidth + ' col-xs-12 syncClick">' + tempHTML + '</div>';
        }


        tempHTML = '';
        if (((parts_array[1] != '') && (parts_array[1] != null)) || ((parts_array[0] != '') && (parts_array[0] != null)) || ((parts_array[2] != '') && (parts_array[2] != null))) {
            if ($.isArray(parts_array[1][0])) {
                for (var i = 0; i < parts_array[1].length; i++) {
                    if (parts_array[1][i][0].indexOf("activityText") >= 0) {
                        tempHTML += createActText(parts_array[1][i][0], parts_array[1][i][1])
                    } else if (parts_array[1][i][0].indexOf("image") >= 0) {
                        var caption = '';
                        if ((parts_array[1][i][2] != '') && (parts_array[1][i][2] != null)) {
                            caption = '<p>' + parts_array[1][i][2] + '</p>';
                        }
                        tempHTML += '<div style="max-width: 100%; min-width: 7em;" class="figure full ' + ((parts_array[1][i] != '') && (parts_array[1][i] != null) ? parts_array[1][i][0] : '') + '">' + ((parts_array[1][i] != '') && (parts_array[1][i] != null) ? parts_array[1][i][1] : '') + caption + '</div>';

                    } else if (parts_array[1][i][0].indexOf("video") >= 0) {
                        tempHTML += '<div style="max-width: 100%; min-width: 7em;" class="' + ((parts_array[1][i] != '') && (parts_array[1][i] != null) ? parts_array[1][i][0] : '') + '" data-url="' + ((parts_array[1][i] != '') && (parts_array[1][i] != null) ? parts_array[1][i][1] : '') + '">' + videoHTML + '</div>';
                    } else if (parts_array[1][i][0].indexOf("syncText") >= 0) {
                        tempHTML += '<div id="' + parts_array[1][i][1] + '"></div>'
                    } else {
                        tempHTML += '<div class="' + ((parts_array[1][i] != '') && (parts_array[1][i] != null) ? parts_array[1][i][0] : '') + '">' + ((parts_array[1][i] != '') && (parts_array[1][i] != null) ? parts_array[1][i][1] : '') + '</div>';
                    }
                }
            } else {
                tempHTML = '<div class="' + ((parts_array[1] != '') && (parts_array[1] != null) ? parts_array[1][0] : '') + '">' + ((parts_array[1] != '') && (parts_array[1] != null) ? parts_array[1][1] : '') + '</div>';
            }
            theHTML += '<div class="col-md-' + centerWidth + ' col-xs-12 syncClick">' + tempHTML + '</div>';
        }



        tempHTML = '';
        if ((parts_array[2] != '') && (parts_array[2] != null)) {
            if ($.isArray(parts_array[2][0])) {
                for (var i = 0; i < parts_array[2].length; i++) {
                    if (parts_array[2][i][0].indexOf("image") >= 0) {
                        var caption = '';
                        if ((parts_array[2][i][2] != '') && (parts_array[2][i][2] != null)) {
                            caption = '<p>' + parts_array[2][i][2] + '</p>';
                        }
                        tempHTML += '<div style="max-width: 100%; min-width: 7em;" class="figure full ' + ((parts_array[2][i] != '') && (parts_array[2][i] != null) ? parts_array[2][i][0] : '') + '">' + ((parts_array[2][i] != '') && (parts_array[2][i] != null) ? parts_array[2][i][1] : '') + caption + '</div>';
                    } else if (parts_array[2][i][0].indexOf("syncText") >= 0) {
                        tempHTML += '<div id="' + parts_array[2][i][1] + '"></div>'
                    } else {
                        tempHTML += '<div class="' + parts_array[2][i][0] + '">' + parts_array[2][i][1] + '</div>';
                    }
                }
            } else {
                if (parts_array[2][0].indexOf("image") >= 0) {
                    var caption = '';
                    if ((parts_array[2][2] != '') && (parts_array[2][2] != null)) {
                        caption = '<p>' + parts_array[2][i][2] + '</p>';
                    }
                    tempHTML += '<div style="max-width: 100%; min-width: 7em;" class="figure full ' + ((parts_array[2] != '') && (parts_array[2] != null) ? parts_array[2][0] : '') + '">' + ((parts_array[2] != '') && (parts_array[2] != null) ? parts_array[2][1] : '') + caption + '</div>';
                } else if (parts_array[2][0].indexOf("syncText") >= 0) {
                    tempHTML += '<div id="' + parts_array[2][1] + '"></div>'
                } else {
                    tempHTML += '<div class="' + parts_array[2][0] + '">' + parts_array[2][1] + '</div>';
                }
            }
            theHTML += '<div class="col-md-' + rightWidth + ' col-xs-12 syncClick">' + tempHTML + '</div>';
        }

        // Inserts the ROWS
        if (theHTML != "") {
            theHTML = '<div class="row">' + theHTML + '</div>';
        }

        return theHTML;
    }



    //public functions

    this.checkAnswers = function (myItem) {


        if ($("#box1").find($(".fa")).hasClass( "fa-check-square-o" )){
            // $("#popBox2").fadeIn("slow");
            $("#popBox1").css('opacity', 1);
        }

        if ($("#box2").find($(".fa")).hasClass( "fa-check-square-o" )){
            // $("#popBox2").fadeIn("slow");
            $("#popBox2").css('opacity', 1);
        }

        if ($("#box3").find($(".fa")).hasClass( "fa-check-square-o" )){
            // $("#popBox3").fadeIn("slow");
            $("#popBox3").css('opacity', 1);
        }

        
        var score = 0;
        var percentage = 100;
        var temp = {
            "score": score,
            "percentage": percentage
        };
        return temp;

        
    }


    var pressed = false;
    this.showAnswers = function () {
        if (!pressed) {
            pressed = true;
        } else {
            pressed = false;
        }
    };

    //public functions
    this.disableActivity = function() {
        console.log("disableActivity > run from contentcreator.js")
    }

    //initialize Object....
    __construct(this);
}
