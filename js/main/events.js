function toggleSequence(theDirection) {
    //alertMe("! toggleSequence")

    
    
    /* Stop the AudioPlayer */
    player.stop();
    player.unload();
    
    
    
    var linkElem = acc.linkID.split("_");
    var newLinkID = "";
    var exNum = linkElem[linkElem.length - 1];
    var positions_array = [];
    var children_array = [];
    var theLengthToGo;
    var i;

    if (theDirection == "next") {
        consoleMe("next")
        /* Deactivate of buttons wasn't working on Chrome so had to add the following code...*/
        var myPosition = acc.menuObj.menu;

        for (i = 0; i < linkElem.length - 1; i++) {
            myLevel = "level" + (i * 1 + 1);
            positions_array.push(myPosition[myLevel]);
            children_array.push(Object.size(myPosition[myLevel]));
            myPosition = myPosition[myLevel][linkElem[i]];
            //myPosition = myPosition[myLevel][linkElem[i]]["level"+(i+2)]
        }
        myLevel = "level" + (i * 1 + 1);
        myPosition = myPosition[myLevel];

        if (exNum == Object.size(myPosition) - 1) {

            for (i = linkElem.length - 2; i >= 0; i--) {
                if (linkElem[i] * 1 < children_array[i] - 1) {
                    linkElem[i] = linkElem[i] * 1 + 1;
                    if (positions_array[i][linkElem[i]].type == undefined) {
                        //consoleMe("213 - type==undefined")
                        linkElem[i + 1] = 0;
                        theLengthToGo = i + 1
                        if (positions_array[i][linkElem[i] * 1]["level" + (i * 1 + 2)][linkElem[i + 1]].type == undefined) {
                            linkElem[i + 2] = 0;
                            theLengthToGo = i + 2
                        } else {
                            if (linkElem[i + 2]) {
                                linkElem.splice(i + 2, 1);
                            }
                        }
                        exNum = 0;
                        break;
                    } else {
                        theLengthToGo = i;
                        linkElem.splice(i + 1, linkElem.length - (i + 1));
                        //consoleMe("226 - theLengthToGo "+theLengthToGo)
                        //consoleMe("227 - linkElem[i] "+linkElem[i])
                        break;
                    }
                }
            }
            if (theLengthToGo == undefined)
                exNum = "end";

        } else {
            theLengthToGo = linkElem.length - 1;
            linkElem[theLengthToGo]++;
            exNum++;

            if (myPosition[linkElem[theLengthToGo]].type == undefined) {
                exNum = 0;
                linkElem.push(exNum);
            }
        }

        if (exNum == "home") {
            return;
        }
        if (exNum == "end") {
            document.getElementById("contentDIV").scrollTop = 0;
            document.getElementById("contentDIV").style.overflow = "hidden";
            document.getElementById("theTitles_unit_group").innerHTML = '';
            document.getElementById("theTitles_lesson_title").innerHTML = '';
            loadExercise("end");
            cleanSelections();
            updateContent("end");
            // MENU UPDATE    
            updateMenu("end");
            return;
        }
        /* */
    } else {
        /* Deactivate of buttons wasn't working on Chrome so had to add the following code...*/
        var myPosition = acc.menuObj.menu;
        consoleMe("--------myPosition--------" + myPosition)
        for (i = 0; i < linkElem.length - 1; i++) {
            myLevel = "level" + (i * 1 + 1);
            positions_array.push(myPosition[myLevel]);
            children_array.push(Object.size(myPosition[myLevel]));
            myPosition = myPosition[myLevel][linkElem[i]];
            //myPosition = myPosition[myLevel][linkElem[i]]["level"+(i+2)]
        }
        myLevel = "level" + (i * 1 + 1);
        myPosition = myPosition[myLevel];

        consoleMe("261 - myPosition " + myPosition)
        if (exNum == 0) {
            for (i = linkElem.length - 1; i >= 0; i--) {
                if (linkElem[i] * 1 > 0) {
                    linkElem[i] = linkElem[i] * 1 - 1;
                    if (positions_array[i][linkElem[i]].type == undefined) {
                        linkElem[i + 1] = positions_array[i][linkElem[i]]["level" + (i * 1 + 2)].length - 1;
                        theLengthToGo = i + 1;
                        if (positions_array[i][linkElem[i] * 1]["level" + (i * 1 + 2)][linkElem[i + 1]].type == undefined) {
                            linkElem[i + 2] = positions_array[i][linkElem[i] * 1]["level" + (i * 1 + 2)][linkElem[i + 1]]["level" + (i * 1 + 3)].length - 1;
                            theLengthToGo = i + 2;
                            exNum = positions_array[i][linkElem[i] * 1]["level" + (i * 1 + 2)][linkElem[i + 1]]["level" + (i * 1 + 3)].length - 1;
                        } else {
                            exNum = positions_array[i][linkElem[i] * 1]["level" + (i * 1 + 2)].length - 1;
                        }
                        consoleMe("------exNum--------" + exNum)
                        consoleMe("linkElem.length " + linkElem.length);
                        consoleMe("linkElem " + linkElem);
                        if (linkElem.length == 3) {
                            if (linkElem[2] == 0) {
                                linkElem.length = 2;
                            }
                        }
                        break;
                    } else {
                        theLengthToGo = i;
                        exNum = linkElem[i] * 1;
                        linkElem.splice(i + 1, linkElem.length - (i + 1));
                        consoleMe("linkElem.length " + linkElem.length);
                        consoleMe("linkElem " + linkElem);
                        break;
                    }
                }
            }
            // return;
        } else {

            theLengthToGo = linkElem.length - 1;
            linkElem[theLengthToGo]--;
            exNum--;

            if (myPosition[linkElem[theLengthToGo]].type == undefined) {
                consoleMe("no exer");
                exNum = myPosition[linkElem[theLengthToGo]]["level" + (theLengthToGo * 1 + 2)].length - 1;
                linkElem.push(exNum);
            }



            if (theLengthToGo == 0) {
                positions_array = [];
                children_array = [];
                myPosition = acc.menuObj.menu;
                myLevel = "level1";
                positions_array.push(myPosition[myLevel]);
                children_array.push(Object.size(myPosition[myLevel]));
                if (myPosition[myLevel].length > 0) {
                    myPosition = myPosition[myLevel][linkElem[theLengthToGo]];
                    myLevel = "level2";
                    if (myPosition[myLevel]) {
                        positions_array.push(myPosition[myLevel]);
                        children_array.push(Object.size(myPosition[myLevel]));
                        linkElem[1] = Object.size(myPosition[myLevel]) - 1;
                        myPosition = myPosition[myLevel][Object.size(myPosition[myLevel]) - 1];
                        exNum = linkElem[1];
                        myLevel = "level3";
                        theLengthToGo = 1;
                        if (myPosition[myLevel]) {
                            positions_array.push(myPosition[myLevel]);
                            children_array.push(Object.size(myPosition[myLevel]));
                            linkElem[2] = Object.size(myPosition[myLevel]) - 1;
                            myPosition = myPosition[myLevel][Object.size(myPosition[myLevel]) - 1];
                            exNum = linkElem[2];
                            theLengthToGo = 2;

                        }
                    }
                }
            }
        }
        if (exNum == "home") {
            return;
        }
        /*  */

    }
    if (linkElem.length == 1) {
        /* Classes For Main Menu 
        hideTheMenu();
        */
        exNum = linkElem[0];
        newLinkID = exNum + "";
    } else if (linkElem.length == 2) {
        /* Classes For Main Menu 
        hideTheMenu();
        */
        newLinkID = linkElem[0];
        exNum = linkElem[1]
        newLinkID += "_" + exNum;
        //consoleMe("331-linkElem.length == 2 newLinkID   "+newLinkID);
    } else if (linkElem.length == 3) {
        /* Classes For Main Menu 
        hideTheMenu();
        */
        newLinkID = linkElem[0];
        exNum = linkElem[2]
        newLinkID += "_" + linkElem[1] + "_" + exNum;
        //consoleMe("331-linkElem.length == 3 newLinkID   "+newLinkID);
    } else {
        newLinkID = linkElem[0];
        for (i = 1; i < theLengthToGo; i++) {
            newLinkID += "_" + linkElem[i];
        }
        newLinkID += "_" + exNum;
    }
    
    
    /* NEW - Below the first <<if>> calls loadExercise DIRECTLY BUT then the #menuEx Event fires again in order to UPDATE the Navigation (prev/next)
    * SO I DISACTIVATE the direct call to loadExercise
    
    
    if (myTypeIs(newLinkID) == "exercise") {
        loadExercise(newLinkID);
    }
    */
    
    
    var currentLiNode = $("li[data-id='" + newLinkID + "']")[0];
    eventFire(currentLiNode, "click");
    
}

/**
 the function that runs when a button is selected. Depending on the button that
 was clicked, appropriate action is taken
 **/
function delegateButtonClicks(evt, that, theSenderName, locObj) {
    //alertMe("delegateButtonClicks")

    if (!locObj.theTarget.disabled) {
        // consoleMe("delegateButtonClicks> evt: " + evt + " that: " + that + " theSenderName: " + theSenderName + "locObj.theTarget : " + locObj.theTarget + "locObj.theTargetType: " + locObj.theTargetType);

        //var theSource = locObj.theTargetName;
        // The Image of Button
        //var str = locObj.theTarget.firstChild;

        switch (theSenderName) {
            case "previous":
            case "next":
                toggleSequence(theSenderName);
                break;
            case "notesb":
                handleNotes(theSenderName);
                break;
            case "goBack":
                UpALevel();
                break;
            case "audiopl":
            case "audiorew":
            case "audiofor":
                handleAudio(theSenderName);
                break;
            case "tapescr":
                tapescriptShow(theSenderName);
                break;
            case "checkB":
            case "showB":
            case "resetB":
            case "showtext":
            case "showjust":
            case "showgrammar":
            case "showLF":
            case "showUE":
            case "showCU":
            case "videoB":
            case "showaudio":
                assignFooterExtraTextButtons(locObj);
                break;
            case "showgrade":
                $("#grade").toggle();
                break;
            default:
        }
    }
}
