/* Gets Data from the settings.JSON and makes sure the defined buttons are visible - > "buttonsOn" : i.e. ["previous", "next"] */
function keepCorrectButtons(setts) {
    
    //alert(Object.size(setts.buttonsOn))
    var localProps = [];
    for (var i = 0; i < Object.size(setts.buttonsOn); i++) {
        try {
            removeClass(document.getElementsByName(setts.buttonsOn[i])[0], 'displayNone');
        } catch (err) {
        }
    }
	if (hasClass(document.getElementById("showgrade"), "displayNone")){
		document.getElementById("seperator0").style.display = "none";
	}
}


function buildGui(setts) {
    var localProps = [];
    for (var name in setts.buttonLabels) {
        localProps.push(name);
    }
    for (var i = 0; i < Object.size(setts.buttonLabels); i++) {
        try {
            document.getElementsByName(localProps[i])[0].value = setts.buttonLabels[localProps[i]];
        } catch (err) {
            // if(window.console && console.error("buildGui Error:" + err));
        }
    }
    return setts;
}









// EXTRA TEXT BUTTONS ********************************************************

// CrossBrowser (IE) to find object content 
function normalisedXtraButtons() {

    /*var x = document.getElementById("exercise");*/
    var x = document.getElementById("activityIFrame");
    var y = (x.contentWindow || x.contentDocument);
    if (y.document)y = y.document;

    try {
        var ieActivity = y.frames.activity;
    } catch (err) {
        var allOtherActivity = window.frames[0].activity;
    }

    if (ieActivity) {
        var normalisedActivity = ieActivity;
    } else {
        var normalisedActivity = allOtherActivity;
    }

    return normalisedActivity;
}

function normalisedB4ObjTag() {

    /*var x = document.getElementById("exercise");*/
    var x = document.getElementById("activityIFrame");

    var y = (x.contentWindow || x.contentDocument);
    if (y.document)y = y.document;

    try {
        var ieReference = y.frames.activity;
    } catch (err) {
        var allOtherReference = window.frames[0];
    }

    if (ieReference) {
        var normalisedReference = y.frames;
    } else {
        var normalisedReference = allOtherReference;
    }

    return normalisedReference;
}

// THE LINKS/BUTTONS
function assignFooterExtraTextButtons(locObj) {


    var normalisedReference = normalisedB4ObjTag();

    //debugger;
    if (normalisedReference.spa) {
        var theLocObj = locObj;

        var theSource = theLocObj.theTargetName;
        var str = theLocObj.theTarget.firstChild;
        var res;

        if (str.src.indexOf("On") === -1 && theSource !== "checkB") {

            setExtraTextButtons();
            res = str.src.replace("Off", "On");
        } else {

            res = str.src.replace("On", "Off");
        }

        // CHANGE THE SRC of the Button's Img
        str.src = res;

        // Disable current Button During Animation (set in extraText.js)
        theLocObj.theTarget.disabled = true;

        var myButts = document.getElementsByClassName("mainButtons")[0];
        for (var i = 0; i < myButts.children.length; i++) {
            try {
                if (myButts.children[i].firstChild.firstChild.src.indexOf("On") !== -1 || myButts.children[i].firstChild.firstChild.src.indexOf("Off") !== -1) {
                    myButts.children[i].firstChild.disabled = true;
                }
            } catch (err) {
                //consoleMe(" Disable Failed:" + err);
            }

        }

        // Enable current Button During Animation (set in extraText.js)
        setTimeout(function () {
            //theLocObj.theTarget.disabled = false;

            for (var i = 0; i < myButts.children.length; i++) {
                try {
                    if (myButts.children[i].firstChild.firstChild.src.indexOf("On") !== -1 || myButts.children[i].firstChild.firstChild.src.indexOf("Off") !== -1) {
                        myButts.children[i].firstChild.disabled = false;
                    }
                } catch (err) {
                    //consoleMe(" Disable Failed:" + err);
                }

            }

        }, globalTimerExtraText);

        // Change Button Graphic (ON/OFF)			
        switch (theSource) {
            case "checkB" :
                closeVideo();
                normalisedReference.$("#checkAnswers").click();

                /* DISABLE SHOW ALL
                 theOtherBut = document.getElementById("showB");
                 
                 if(theOtherBut.firstChild.src.indexOf("On") === -1 && theOtherBut.firstChild.src.indexOf("Off") === -1){
                 theOtherBut.disabled = false;
                 theOtherBut.firstChild.src = theOtherBut.firstChild.src.replace("Dis", "Off");					
                 } else {
                 theOtherBut.disabled = true;
                 theOtherBut.firstChild.src = theOtherBut.firstChild.src.replace("Off", "Dis");
                 theOtherBut.firstChild.src = theOtherBut.firstChild.src.replace("On", "Dis");	
                 }	
                 */

                break;
            case "showB" :
                closeVideo();
                normalisedReference.$("#showAnswers").click();

                //* DISABLE CHECK ALL
                theOtherBut = document.getElementById("checkB");

                if (theOtherBut.firstChild.src.indexOf("On") === -1 && theOtherBut.firstChild.src.indexOf("Off") === -1) {
                    theOtherBut.disabled = false;
                    theOtherBut.firstChild.src = theOtherBut.firstChild.src.replace("Dis", "Off");
                } else {
                    theOtherBut.disabled = true;
                    theOtherBut.firstChild.src = theOtherBut.firstChild.src.replace("Off", "Dis");
                    theOtherBut.firstChild.src = theOtherBut.firstChild.src.replace("On", "Dis");
                }
                //*/

                break;
            case "resetB" :
                closeVideo();
                
                /*Cookie save skipped below.*/
                /*
                normalisedReference.spa.activityJSON.selections = {};
                normalisedReference.spa.activityJSON.score = "";
                normalisedReference.spa.activityJSON.percentage = "";
                normalisedReference.spa.saveCookie();
                */
                
                normalisedReference.location.reload();//this need to becomne reset now...
                break;
            case "showtext" :
                closeVideo();
                normalisedReference.$("#ShowText").click();
                break;
            case "showjust" :
                closeVideo();
                normalisedReference.$("#ShowJust").click();
                break;
            case "showgrammar" :
                closeVideo();
                normalisedReference.$("#ShowGrammar").click();
                break;
            case "showLF" :
                closeVideo();
                normalisedReference.$("#ShowLF").click();
                break;
            case "showUE" :
                closeVideo();
                normalisedReference.$("#ShowUE").click();
                break;
            case "showCU" :
                closeVideo();
                normalisedReference.$("#ShowCU").click();
                //normalisedReference.$("#ShowGrammar")[0].click();
                break;
            case "videoB" :
                //normalisedReference.$("#ShowVideo").click();					
                //alertMe(normalisedReference.activity.elements["showvideo"]+" "+acc.menuObj+" "+acc.linkID+" "+acc.globalSettings+" "+acc.jsObjExtras);

                // CHANGED to run from Main
                var myactivityVideoParams = "0@activityVideo@" + acc.linkID;
                if (str.src.indexOf("On") === -1) {
                    closeVideo();
                } else {
                    loadVideo(myactivityVideoParams);
                }

                var theExtraTextDivExt = normalisedReference.document.getElementById('extraTextDiv');
                normalisedReference.enableAllButs();
                normalisedReference.animateExtraHide(theExtraTextDivExt, this);

                break;
            case "showaudio" :
                closeVideo();
                normalisedReference.$("#ShowAudio").click();
                break;
            default :
                "";
        }
    }
}

// USED IN updateContent + loadExercise
function resetExtraTextButtons() {

    //alertMe("resetExtraTextButtons");

    var theMainButs = document.getElementsByClassName("mainButtons")[0].children;
    var theMainButsLength = theMainButs.length;

    for (i = 0; i < theMainButsLength; i++) {

        var theBut = theMainButs[i].firstChild;
        var theButName = theMainButs[i].firstChild.name;
        var theImg = theMainButs[i].firstChild.firstChild;

        switch (theButName) {
            case "checkB" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/check_Dis.png';
                break;
            case "showB" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/show_Dis.png';
                break;
            case "resetB" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/reset_Dis.png';
                break;
            case "showtext" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/showText_Dis.png';
                break;
            case "showjust" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/justify_Dis.png';
                break;
            case "showgrammar" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/g_Dis.png';
                break;
            case "showLF" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/lf_Dis.png';
                break;
            case "showUE" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/ue_Dis.png';
                break;
            case "showCU" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/cu_Dis.png';
                break;
            case "videoB" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/vid_Dis.png';
                break;
            case "showaudio" :
                theBut.disabled = true;
                theBut.style.cursor = "default";
                theImg.src = 'resources/layout/footer/BtnTxt_Dis.png';
                document.getElementById("rangevalue").value = "";
                break;
            default :

        }
    }
}





// USED IN
function setExtraTextButtons() {

    //alertMe("setExtraTextButtons");
    //closeVideo();	

    var theMainButs = document.getElementsByClassName("mainButtons")[0].children;
    var theMainButsLength = theMainButs.length;

    var normalisedActivity = normalisedXtraButtons();

    for (i = 0; i < theMainButsLength; i++) {

        var theBut = theMainButs[i].firstChild;
        var theButName = theMainButs[i].firstChild.name;
        var theImg = theMainButs[i].firstChild.firstChild;

        switch (theButName) {
            case "checkB" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/check_Dis.png';
                 */
                if (normalisedActivity.ex[0].settings.typeOfCheck.checkButton && normalisedActivity.ex[0].settings.typeOfCheck.checkButton !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/check_Off.png';
                }
                break;
            case "showB" :
                if (normalisedActivity.ex[0].settings.typeOfCheck.checkButton || (normalisedActivity.ex[0].settings.typeOfCheck.showAll && normalisedActivity.ex[0].settings.typeOfCheck.showAll !== undefined)) {
                    //if (normalisedActivity.ex[0].settings.typeOfCheck.showAll && normalisedActivity.ex[0].settings.typeOfCheck.showAll !== undefined){
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/show_Off.png';
                }
                break;
            case "resetB" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/reset_Dis.png';
                 */
                theBut.disabled = false;
                theBut.style.cursor = "pointer";
                theImg.src = 'resources/layout/footer/reset_Off.png';
                break;
            case "showtext" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/showText_Dis.png';
                 */
                if (normalisedActivity.elements.showtext !== '' && normalisedActivity.elements.showtext !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/showText_Off.png';
                }
                break;
            case "showjust" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/showText_Dis.png';
                 */
                if (normalisedActivity.elements.showjusttext !== '' && normalisedActivity.elements.showjusttext !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/justify_Off.png';
                }
                break;
            case "showgrammar" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/g_Dis.png';
                 */
                if (normalisedActivity.elements.showgrammar !== '' && normalisedActivity.elements.showgrammar !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/g_Off.png';
                }
                break;
            case "showLF" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/lf_Dis.png';
                 */
                if (normalisedActivity.elements.showLF !== '' && normalisedActivity.elements.showLF !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/lf_Off.png';
                }
                break;
            case "showUE" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/ue_Dis.png';
                 */
                if (normalisedActivity.elements.showUE !== '' && normalisedActivity.elements.showUE !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/ue_Off.png';
                }
                break;
            case "showCU" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/cu_Dis.png';
                 */
                if (normalisedActivity.elements.showCU !== '' && normalisedActivity.elements.showCU !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/cu_Off.png';
                }
                break;
            case "videoB" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/vid_Dis.png';
                 */
                if (normalisedActivity.elements.showvideo !== '' && normalisedActivity.elements.showvideo !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/vid_Off.png';
                }
                break;
            case "showaudio" :
                /*
                 theBut.disabled = true;	
                 theBut.style.cursor = "default";
                 theImg.src = 'resources/layout/footer/BtnTxt_Dis.png';
                 */
                if (normalisedActivity.elements.showaudio !== '' && normalisedActivity.elements.showaudio !== undefined) {
                    theBut.disabled = false;
                    theBut.style.cursor = "pointer";
                    theImg.src = 'resources/layout/footer/BtnTxt_Off.png';
                }
                break;
            default :

        }
    }
}

