// GLOBAL TIME for extra show/hide
var globalTimerExtraText = 500;

var acc = {}; //namespace
acc.menuObj = {};
acc.jsObjExtras = {};

/************** SETTINGS********************/
acc.DEFLANG = settings.defaultLanguage;
acc.myLang = (document.querySelector("html").lang != "" ? document.querySelector("html").lang : acc.DEFLANG);
acc.myDir = (document.querySelector("html").dir != "" ? document.querySelector("html").dir : "ltr");
acc.myVersion = (getDataAttribute(document.querySelector("html"), "version") != "" ? getDataAttribute(document.querySelector("html"), "version") : "teacher");
acc.pageAppearance = (settings.pageAppearance != "" ? settings.pageAppearance : "height");
acc.levelExpands = (settings.firstLevelJustExpands != "" ? settings.firstLevelJustExpands : false);
acc.lvlOfAccDevelopment = (settings.lvlOfAccDevelopment != "" ? settings.lvlOfAccDevelopment : "full");
acc.hiddenAccordion = (settings.hiddenAccordion != "" ? settings.hiddenAccordion : false);
acc.completePage = (settings.completePage != "" ? settings.completePage : false);

/*****************PATHS********************/
acc.RESOURCES_FOLDER = settings.mediaLocation.resources;
acc.ACTIVITIES_FOLDER = settings.mediaLocation.activities;
acc.VERSION = (settings.version != "" ? settings.version : "teacher");
acc.DISTINCTIVE_TITLE = (settings.distinctiveTitle != "" ? settings.distinctiveTitle : "HTMLproject");
acc.TOOLBAR_EXE_ONLY = (settings.toolbarExeOnly != "" ? settings.toolbarExeOnly : false);

acc.currentActFolder = '';
acc.scrollerReference = '';


acc.globalSettings = {};

/**************USER INFO********************/
acc.userInfo = {};
acc.state = {};
acc.unitState = {};
acc.infoJSON = {};


/***************count pages*****************/
acc.unitPages;
acc.pageNo;
acc.showLastPage = 0;

// NEW > Numbering of Activities - Current [Saved in data-page attribute of Menu Link]
acc.actnumunit = 0;
acc.actnumwhole = 0;

/* How Deep the Menu goes */
acc.DEPTH_COUNT = 2;





/*************** INIT APP *****************/
setDataAttribute(document.querySelector("html"), "version", acc.VERSION);
acc.linkID = "home";



/*************** LYKIO REGISTRATION *****************/
function registration(userInfo, state, unitState, error) {

    alertMe("!registration")

    if (error) {} else {
        acc.userInfo = userInfo;
        acc.state = state;
        acc.unitState = unitState;
        consoleMe("userInfo - " + acc.userInfo);
        consoleMe("state - " + acc.state);
        consoleMe("unitState - " + acc.unitState);
        if ((acc.state == "") || (acc.state == null)) {
            acc.state = "0_0_0";
        } else {
            $('li[data-id="' + acc.state + '"]').click();
        }
    }
}

/* DOCUMENT READY */

$(document).ready(function () {
    

    
    /* Classes For Main Menu - Bootstrap Tooltip*/
    $('[data-toggle="tooltip"]').tooltip();




    // Read Menu Structure + createEachLevel(HTML)
    createMainHTML(readXML());

    // Read and Count every "type":"exercise" in menu.js (txtMenu.menu)
    acc.unitPages = countExercises(txtMenu.menu, acc.DEPTH_COUNT);

    /* ASSIGN Burger + Menu Buttons */
    assign_All_Menu_Btns();





    /* New - Used instead: assign_All_Menu_Btns
    var myLength;
    var listItemsNode = document.querySelectorAll('nav > ul.accClass > li');
    for (i = 0; i < listItemsNode.length; i++) {
        myLength = listItemsNode[i].childNodes.length;
        if (myLength > 2) {
            listItemsNode[i].addEventListener("click", menuUnitsClick);
            /* Classes For Main Menu
           listItemsNode[i].childNodes[myLength - 1].addEventListener("click", showHideChildren);
            
        }
    }

    listItemsNode = document.querySelectorAll("nav > ul.accClass > ul.accClass > li");
    for (i = 0; i < listItemsNode.length; i++) {
        myLength = listItemsNode[i].childNodes.length;
        if (myLength > 2) {
            listItemsNode[i].addEventListener("click", menuUnitsClick);
            /* Classes For Main Menu
            listItemsNode[i].childNodes[myLength - 1].addEventListener("click", showHideChildren);
           
        }
    }
    listItemsNode = document.querySelectorAll("nav > ul.accClass > ul.accClass > ul.accClass > li");
    for (i = 0; i < listItemsNode.length; i++) {
        myLength = listItemsNode[i].childNodes.length;
        if (myLength > 2) {
            listItemsNode[i].addEventListener("click", level2ndClick);
            /* Classes For Main Menu
            listItemsNode[i].childNodes[myLength - 1].addEventListener("click", showHideChildren);
            
        }
    }

*/
    
    // In Cle without the menu REMAINS GREY
    //$('nav > ul:eq(0)').show('fast');

    //remove unecessary buttons, as defined in the settings
    //keepCorrectButtons(settings);


    //label buttons, as defined in the settings
    // NEW - I do no think it is used too much!!!
    //acc.globalSettings = buildGui(settings);


    // EVENT FOR PREVIOUS/NEXT
    document.getElementById("navDIV").addEventListener("click", function (e) {

        var locObj = crossBrowserSourceElement(e, "submit");

        if (locObj.condtn) {
            delegateButtonClicks(e, this, locObj.theTargetName, locObj);
        } else {
            //nothing
        }
    });
    
    
    $("button").hover(function(){
        $(this).addClass("animated tada");
    }, function(){
        $(this).removeClass("animated tada");
    });
    
    /*
    $("button").mousedown(function() {
        $(this).addClass("animated makeSmall");
    });
    $("button").mouseup(function() {
        $(this).removeClass("animated makeSmall");
    });
    */
    
    /* TEMP DISABLED - Up to Content Loads
    document.getElementById("buttons").addEventListener("click", function (e) {

        var locObj = crossBrowserSourceElement(e, "submit");

        if (locObj.condtn) {
            delegateButtonClicks(e, this, locObj.theTargetName, locObj);
        } else {
            //nothing
        }
    });
    */









    

    /*
    document.getElementById("previous").disabled = true;
    //document.getElementById("previous").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnPrevious_Dis.png";
    $(document.getElementById("previous")).addClass("navButtonsDisabled");
    
    document.getElementById("next").disabled = true;
    //document.getElementById("next").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnNext_Dis.png";
    $(document.getElementById("next")).addClass("navButtonsDisabled");
    
    /* Classes For Main Menu 
    document.getElementById("previous").style.color="rgba(0, 0, 0, 0) linear-gradient(to bottom, #e8e8e8 0%, #dddddd 50%, #c8c8c8 100%) repeat scroll 0 0";
	document.getElementById("next").style.color="rgba(0, 0, 0, 0) linear-gradient(to bottom, #e8e8e8 0%, #dddddd 50%, #c8c8c8 100%) repeat scroll 0 0";
    

    document.getElementById("counter").value = "";
    document.getElementById("counter").style.cursor = "default";

    /* UP LEVEL INIT
    document.getElementById("goBack").disabled = true;
    document.getElementById("goBack").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnUp_Dis.png";
    $(document.getElementById("goBack")).addClass("navButtonsDisabled");

    if (!acc.TOOLBAR_EXE_ONLY) {
        enableToolbar();
    }
    //loadJSON(function(response) {
    // Parse JSON string into object
    //acc.infoJSON = JSON.parse(response);
    */
    try {
        register(registration);
    } catch (err) {
        //$('li[data-id="0_0"]').click();
    }

    document.querySelector("body").addEventListener("unload", onUnloadApp);

    $("html").click(function () {
        //closeMenuOnFocus()
    })
    

    
    
    //Intro.js : a step-by-step guide
    //runIntroJs();
    
    

    
    
    
    
    
    
    
    // END OF DOCUMENT READY
});
