/***** MENU FUNCTIONS *****/

/* Using the data from readXML() it creates the necessary HTML for the Menu. - Called ONLY Once BUT Is calling itSelf – $(document).ready */
acc.totalActivitiesNum = 0;
function countExercises(theObj, theDepth, theLevel) {

    var exerciseNumber = 0;
    var theLength, newDepth, newLevel;
    if (theLevel == undefined)
        theLevel = 1;
    if (theDepth == 1) {
        if (theObj["level" + theLevel]) {
            exerciseNumber = JSON.stringify(theObj["level" + theLevel]).split('"type":"exercise"').length - 1;
        } else {
            exerciseNumber = JSON.stringify(theObj).split('"type":"exercise"').length - 1;
        }
        
        //** NEW - ADD all activites count / Total number of Activities
        acc.totalActivitiesNum+=exerciseNumber;
        
    } else {
        if (theObj["level" + theLevel]) {
            exerciseNumber = [];
            theLength = theObj["level" + theLevel].length;
            newDepth = theDepth - 1;
            newLevel = theLevel + 1;
            for (var i = 0; i < theLength; i++) {
                exerciseNumber.push(countExercises(theObj["level" + theLevel][i], newDepth, newLevel));
            }
        } else {
            exerciseNumber = JSON.stringify(theObj["level" + theLevel]).split('"type":"exercise"').length - 1;
        }
    }

    consoleMe("countExercises(theObj: " + theObj + " theDepth: " + theDepth + " theLevel: " + theLevel + ") - exerciseNumber: " + exerciseNumber)



    return exerciseNumber;
}


/***** GLOBAL FUNCTIONS *****/
function hasClass(el, name) {

    try {
        return new RegExp('(\\s|^)' + name + '(\\s|$)').test(el.className);
    } catch (err) {
        return false;
    }


    consoleMe("hasClass - el: " + el + " name: " + name)
}

function removeClass(el, name) {


    if (hasClass(el, name)) {
        el.className = el.className.replace(new RegExp('(\\s|^)' + name + '(\\s|$)'), ' ').replace(/^\s+|\s+$/g, '');
    }

    consoleMe("removeClass - el: " + el + " name: " + name)
}






/* UNCHECKED */

function eventFire(el, etype) {
    consoleMe("eventFire")

    if (el.dispatchEvent) {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    } else {
        var evt = document.createEventObject();
        el.fireEvent('on' + etype, evt);
    }
}

function findUpTag(el, tag) {
    consoleMe("findUpTag")

    if (el.tagName.toLowerCase() != tag) {
        while (el.parentNode) {
            el = el.parentNode;
            if (el.tagName.toLowerCase() === tag)
                return el;
        }
        return null;
    }
    return el;
}



function disableToolbar() {
    consoleMe("disableToolbar")

    /*$("#tbpoint").click();
	$("#zoom-reset").click();
    var allTheTools = HTMLCollectionToArray(document.getElementsByClassName("toolBarButtons")[0].getElementsByClassName("tool"));
    for (var i = 0; i < allTheTools.length - 1; i++) {
        allTheTools[i].className += (" disabled");
    }	*/
}


function enableToolbar() {
    consoleMe("enableToolbar")

    /*var allTheTools = HTMLCollectionToArray(document.getElementsByClassName("toolBarButtons")[0].getElementsByClassName("disabled"));
    for (var i = 0; i < allTheTools.length; i++) {
        allTheTools[i].classList.remove("disabled");
    }*/
}


function hideTheMenu() {
    consoleMe("** hideTheMenu **")

    var allmenuXpands = document.querySelectorAll('nav .menuXpand');
    for (var i = 0; i < allmenuXpands.length; i++) {
        if (allmenuXpands[i].innerHTML != "") {
            allmenuXpands[i].innerHTML = '<img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXplus.png">';
        }
    }
    $('nav > ul > ul > ul > li > .menuXpand').removeClass("listShown");
    $('nav > ul > ul > ul > li > .menuXpand').addClass("listHidden");
    $('nav > ul > ul > ul > ul').slideUp(300);

    $('nav > ul > ul > li > .menuXpand').removeClass("listShown");
    $('nav > ul > ul > li > .menuXpand').addClass("listHidden");
    $('nav > ul > ul > ul').slideUp(300);

    $('nav > ul > li > .menuXpand').removeClass("listShown");
    $('nav > ul > li > .menuXpand').addClass("listHidden");
    $('nav > ul > ul').slideUp(300);

    /* 
    $('nav > ul > li .menuXpand').removeClass("listShown");
    $('nav > ul > li .menuXpand').addClass("listHidden");
    $('nav > ul > li ul').slideUp(800); 
    */
    //$('nav > ul > li').hide();
}
/*
 
 */
function myTypeIs(linkID) {

    var myType = "undefined";
    var linkElem = linkID.split("_");
    var exNum = linkElem[linkElem.length - 1];
    var myPosition;
    var myLevel;

    myPosition = acc.menuObj.menu;

    for (var i = 0; i < linkElem.length - 1; i++) {
        myLevel = "level" + (i * 1 + 1);
        myPosition = myPosition[myLevel][linkElem[i]];
    }
    myLevel = "level" + (i * 1 + 1);
    myPosition = myPosition[myLevel][linkElem[linkElem.length - 1]];
    if (myPosition.hasOwnProperty('type')) {
        myType = myPosition.type;
    }

    /* DEBUG */
    consoleMe("> myTypeIs: " + myType)

    return myType;
}

function UpALevel() {
    consoleMe("UpALevel")

    var linkElem = acc.linkID.split("_");
    var newLinkID = "";
    var exNum = linkElem[linkElem.length - 1];

    var onlyLi = []; //pairnw mono ta li gia na ginei swsth antistoixish me to menu pou tha ginetai simulate sto click

    //var curIndex = $(locObj.theElement.parentNode).index();

    if (linkElem.length == 0) {
        //consoleMe("never here");
    } else if (linkElem.length == 1) {
        //consoleMe ("go home");
        document.getElementById("goBack").disabled = true;
        removeClass(document.getElementById("goBack"), "navButtonsEnabled");
        document.getElementById("goBack").className += " navButtonsDisabled";
        document.getElementById("goBack").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnUp_Dis.png";

        document.getElementById("contentDIV").scrollTop = 0;
        document.getElementById("contentDIV").style.overflow = "hidden";
        loadExercise("home");
        cleanSelections();
        updateContent("home");
        // MENU UPDATE    
        updateMenu("home");
        document.getElementById("theTitles_unit_group").innerHTML = '';
    } else if (linkElem.length == 2) {
        //consoleMe ("up from lesson to unit");
        for (var i = 0; i < document.getElementsByClassName("accClass")[0].children.length; i++) {
            if (document.getElementsByClassName("accClass")[0].children[i].tagName == "LI") {
                onlyLi.push(document.getElementsByClassName("accClass")[0].children[i]);
            }
        }
        var currentLidata = document.getElementsByClassName(" activeMenuElement")[0].getAttribute("data-id").substr(0, document.getElementsByClassName(" activeMenuElement")[0].getAttribute("data-id").lastIndexOf("_"));
        var currentLiNode = $("li[data-id='" + currentLidata + "']")[0];
        eventFire(currentLiNode, "click");
    } else if (linkElem.length == 3) {
        for (var i = 0; i < document.getElementsByClassName("accClass")[1].children.length; i++) {
            if (document.getElementsByClassName("accClass")[1].children[i].tagName == "LI") {
                onlyLi.push(document.getElementsByClassName("accClass")[1].children[i]);
            }
        }
        var currentLidata = document.getElementsByClassName(" activeMenuElement")[0].getAttribute("data-id").substr(0, document.getElementsByClassName(" activeMenuElement")[0].getAttribute("data-id").lastIndexOf("_"));
        var currentLiNode = $("li[data-id='" + currentLidata + "']")[0];
        eventFire(currentLiNode, "click");
    }
}

function getThePageNumber() {
    consoleMe("getThePageNumber")
    
    //var current = 0;
    var curUnitTotal = 0;
    var linkElem = acc.linkID.split("_");

    switch (acc.DEPTH_COUNT) {
        case 1:
            curUnitTotal = acc.unitPages;
            break;
        case 2:
            curUnitTotal = acc.unitPages[linkElem[0]];
            break;
        case 3:
            curUnitTotal = acc.unitPages[linkElem[0]][linkElem[1]];
            break;
    }
    
    /****
    * NEW - Removed as data-page doesn't function / Exist
    current = getDataAttribute(document.querySelector("li[data-id='" + acc.linkID + "']"), "page");
    ****/
    current = getDataAttribute(document.querySelector("li[data-id='" + acc.linkID + "']"), "actnumwhole");
    
    //return [current, total];
    return [current, curUnitTotal, acc.totalActivitiesNum];
}


function addScroll() {
    consoleMe("addScroll")
    var srollingDIV = "";

    if (window.frames[0]) {
        srollingDIV = $(window.frames[0].document.getElementById("extraTextDiv"));
    } else {
        /*srollingDIV = $(document.getElementById("exercise").contentDocument.frames.document.getElementById("extraTextDiv"));*/
        srollingDIV = $(document.getElementById("activityIFrame").contentDocument.frames.document.getElementById("extraTextDiv"));
    }

    function scroller() {
        try {
            srollingDIV.stop().animate({
                "marginTop": ($(window).scrollTop() + document.getElementById("content_wrapper").scrollTop) + "px"
            }, "slow");
        } catch (err) {
            consoleMe(err);
        }
    };

    //ok. need to keep a reference to the scroller function. need to have a fucntion the first place because in order to remove the
    //listener, you need to know the name. argument.callee notwithstanding. 
    //basically, when you load another activity the listeneres on content keep piling up. trying to remove it (if its anonymous)
    //does not work. trying to remove it by referencing it does not work (since addscroll runs again so the execution context has changed
    //and although a scroller function **IS** found, it is not the original one. So, I keep a reference on the global scope and by using
    //that i can get rid of it on next execution. Mind you the first time (when there is no listener) an exception will be thrown, hence
    //the need for a try catch statement.
    try {
        document.getElementById("content_wrapper").removeEventListener("scroll", acc.scrollerReference); //ANONYMOUS!!!!!! 
        acc.scrollerReference = scroller;
    } catch (err) {
        consoleMe("trying to remove listener, but no exist");
        acc.scrollerReference = scroller;
    }
    document.getElementById("content_wrapper").addEventListener("scroll", acc.scrollerReference);
}

function crossBrowserSourceElement(evnt, theNodeType) {
    consoleMe("crossBrowserSourceElement")

    var fResult = {};
    fResult.condtn = false;
    fResult.theTargetType = "";
    fResult.theTargetName = "";
    fResult.theTarget = "";

    try {
        if (evnt.srcElement) { //chrome property
            //fResult.theTarget = evnt.srcElement;
            fResult.theTarget = findUpTag(evnt.srcElement, "button");

        } else {
            //fResult.theTarget = evnt.originalTarget;
            fResult.theTarget = findUpTag(evnt.originalTarget, "button");
        }

        fResult.theTargetType = fResult.theTarget.type.toLowerCase();
        fResult.theTargetName = fResult.theTarget.name;

        if (fResult.theTargetType != theNodeType) {
            evnt.preventDefault();
        } else {
            fResult.condtn = true;
            //allow bubbling
        }
    } catch (err) {
        //some error handling - usually the click was on the select and it has no name?
        //consoleMe("err " + err);
    }
    /*
     STUPID: 2nd check for nodeName if type doesn't exist based on try.
     Needs revisiting.
     
     Φαντάζομαι αφού θα το ξαναδούμε θα το αφήσω έτσι για τώρα.
     */
    if (fResult.condtn == false) {
        try {
            if (evnt.srcElement) { //chrome property
                fResult.theTargetType = evnt.srcElement.nodeName.toLowerCase();
                fResult.theTargetName = evnt.srcElement.name;
                fResult.theTarget = evnt.srcElement;
            } else {
                fResult.theTargetType = evnt.originalTarget.nodeName.toLowerCase(); //firefox
                fResult.theTargetName = evnt.originalTarget.name;
                fResult.theTarget = evnt.originalTarget;
            }
            if (fResult.theTargetType != theNodeType) {
                evnt.preventDefault();
            } else {
                fResult.condtn = true;
                //allow bubbling
            }
        } catch (err) {
            //some error handling - usually the click was on the select and it has no name?
        }
    }
    return fResult;
}

function handleNotes(myObj) {
    consoleMe("handleNotes")
    if (document.getElementById("notes").style.visibility == "hidden") {
        document.getElementById("notes").style.visibility = "visible";
        document.getElementById("notesb").style.backgroundImage = "url('" + acc.RESOURCES_FOLDER + "/layout/menu/BtnsNotes_Pressed.png')";
    } else {
        document.getElementById("notes").style.visibility = "hidden";
        document.getElementById("notesb").style.backgroundImage = "url('" + acc.RESOURCES_FOLDER + "/layout/menu/BtnsNotes_Idle.png')";
    }
}

function cleanSelections() {
    consoleMe("**cleanSelections**")

    $('nav > ul >  ul >  ul >  ul >  li').removeClass("activeMenuElement");
    $('nav > ul >  ul >  ul >  ul >  li > .MPos').addClass("displayNone");
    $('nav > ul >  ul >  ul >  li').removeClass("activeMenuElement");
    $('nav > ul >  ul >  ul >  li > .MPos').addClass("displayNone");
    $('nav > ul >  ul >  li').removeClass("activeMenuElement");
    $('nav > ul >  ul >  li > .MPos').addClass("displayNone");
    $('nav > ul >  li').removeClass("activeMenuElement");
    $('nav > ul >  li > .MPos').addClass("displayNone");

    $('nav > ul a').css("border", "none");
}



function clickImage(myImage) {
    consoleMe("clickImage")
    var myID = getDataAttribute(myImage, 'id');
    var cleanID = myID;
    if (myID[0] == "p") {
        cleanID = myID.substr(4);
    }
    var currentLiNode = $("li[data-id='" + cleanID + "']")[0];
    eventFire(currentLiNode, "click");
}





function GetFilename(url) {
    consoleMe("GetFilename")

    if (url) {
        var m = url.toString().match(/.*\/(.+?)\./);
        if (m && m.length > 1) {
            return m[1];
        }
    }
    return "";
}

function changeIndex(evt) {
    consoleMe("changeIndex")

    $("nav").addClass("lowerIndex");
    PrefixedEventRemove(document.querySelector("nav"), "AnimationEnd", changeIndex);
}

function insertBorder(theElement) {
    consoleMe("** insertBorder **")
    theElement.style.borderBottom = "2px solid white";
    /*theElement.style.padding = "10px 10px 10px 10px";*/
}

function removeBorder(theElement, currentElement) {
    consoleMe("removeBorder")

    // if Animation happens to another element remove Bookmark's border after animation
    if (theElement != currentElement) {
        theElement.style.border = "none";
        theElement.style.padding = "0";
    }
}

function onUnloadApp(evt) {
    consoleMe("onUnloadApp")
    complete(acc.unitState);
}
