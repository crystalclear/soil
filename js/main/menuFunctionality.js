/************************************************* 
            Menu Data Handling 
*************************************************/


/* Fills Menu, but Also Creates iFrame */

function createMainHTML(theMenuJSON) {

    //consoleMe("! createMainHTML")

    //$("nav").html(theContent.menu);

    $("nav").html(theMenuJSON);


    // Language Feature - Needed?
    var tempLang = (acc.myLang == acc.DEFLANG ? '' : '-' + acc.myLang);

    // Create Object in Main HTML
    theHTML = '<iframe id="activityIFrame" name="activityIFrame" allowfullscreen src="' + acc.ACTIVITIES_FOLDER + '/activity' + tempLang + '.html" ></iframe>';

    $("#myObject").html(theHTML);


    /*
    switch (get_browser()) {
        case "Microsoft Internet Explorer":
        case "Netscape":
            //  document.querySelector("#content-wrapper").style.height = "550px";
            break;
        case "Chrome":
            // document.querySelector("#content-wrapper").style.height = "550px";
            break;
    }
    */
}

/* Read menu.js (JSON Menu Object) */
function readXML() {

    /* 
    var countCorrect; 
    var itemType;
    
    var myLVL1, myLVL2, myLVL3;
    */

    var theXMLElements = '';
    /*
    var theXMLElements = new Object();
    theXMLElements.menu = '';
    
    
    acc.menuObj = JSON.parse(JSON.stringify(txtMenu));

    theXMLElements.menu += '<ul class="accClass">';

    //acc.pageNo = 0;

    // Loop for Everything that is under LEVEL 1
    for (i = 0; i < Object.size(acc.menuObj.menu.level1); i++) {
        theXMLElements.menu += createEachLevel(1, acc.menuObj.menu.level1[i], i);
    }

    theXMLElements.menu += '</ul>';
    */

    theXMLElements += '<ul class="topMenuLevel">';
    acc.menuObj = JSON.parse(JSON.stringify(txtMenu));

    // Loop for Everything that is under LEVEL 1
    for (var i = 0; i < Object.size(acc.menuObj.menu.level1); i++) {
        theXMLElements += createEachLevel(1, acc.menuObj.menu.level1[i], i);
    }



    theXMLElements += '</ul>';




    /* Concerns Different types of Activites (Exercise, Video, audio) - Not really USed Anymore)
    theXMLElements.content = ''; 

    //*****************************

    // SEPARATE EXERCISE DATA TO EXTRA[i.e. video] DATA
    /*
    var exerciseObject = JSON.parse(JSON.stringify(txtMenu));
    var extraObject = JSON.parse(JSON.stringify(txtMenu));


    for (i = 0; i < Object.size(txtMenu.menu.level1); i++) {
        for (j = 0; j < Object.size(txtMenu.menu.level1[i].level2); j++) {
            for (k = 0; k < Object.size(txtMenu.menu.level1[i].level2[j].level3); k++) {
                
                
                
                try {
                    if (txtMenu.menu.level1[i].level2[j].level3[k].type && txtMenu.menu.level1[i].level2[j].level3[k].type === "video" || txtMenu.menu.level1[i].level2[j].level3[k].type && txtMenu.menu.level1[i].level2[j].level3[k].type === "audio") {
                        //consoleMe("txtMenu: "+txtMenu.menu.level1[i].level2[j].level3[k].type +" | "+i+" | "+j+" | "+k)
                        delete exerciseObject.menu.level1[i].level2[j].level3[k];
                    }
                } catch (e) {
                    //consoleMe("error Exercise: " + e);
                }
                
                
                try {
                    if (txtMenu.menu.level1[i].level2[j].level3[k].type && txtMenu.menu.level1[i].level2[j].level3[k].type === "exercise") {
                        //consoleMe("txtMenu: "+txtMenu.menu.level1[i].level2[j].level3[k].type +" | "+i+" | "+j+" | "+k)
                        delete extraObject.menu.level1[i].level2[j].level3[k];
                    }
                } catch (e) {
                    //consoleMe("error Extras: " + e);
                }
                
            }
        }

    }
    
    acc.jsObjExtras = extraObject;
    */


    //******************************
    //var jsonText = "";

    //acc.menuObj = exerciseObject;    


    return theXMLElements;
}

/*
theXMLElements.menu += createEachLevel(1, acc.menuObj.menu.level1[i], i);
lvl = 1
theObj = acc.menuObj.menu.level1[i]
i = Object.size(acc.menuObj.menu.level1) = 6
*/



function createEachLevel(lvl, theObj, myID) {

    var theHTML;
    var nextLevel = lvl + 1;
    //var i;
    var myTitle = localizeText(theObj.title, acc.myLang);
    
    // Reset Activity Numbering Per Unit With Change of Level
    if ((lvl + 1) == acc.DEPTH_COUNT) {
        acc.actnumunit = 0;
    }
    
    
    //var spanTaNeyvr = new RegExp;

    /*
    var searchPattern = /(.*?)\|(.*?)/;
    var replacePattern = '</span><span>$1</span><span class = "whiteMe">$2';
    */

    //var hideLevelClass = '';

    /*
    if (localizeText(theObj.title, acc.myLang).indexOf("|") > 0) {
        myTitle = localizeText(theObj.title, acc.myLang).replace(searchPattern, replacePattern);
        //myTitle = localizeText(theObj.title, acc.myLang).replace('|', '</span><span class = "whiteMe">'); 
    }
    */

    if (theObj.hasOwnProperty("level" + nextLevel)) {

        // theHTML = '<li data-id="' + myID + '" ><div class="unitMenu"><div class="MPos displayNone"><img width="15" height="15" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MPos.png"></div><span class="unitMenuTitle">' + myTitle + '</span><div class="menuXpand listHidden"><img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXplus.png"></div></div>';
        theHTML = '<li id="' + myID + '" class="hasNextLevel" data-id="' + myID + '" data-actnumunit="' + acc.actnumunit + '" data-actnumwhole="' + acc.actnumwhole + '" ><div class="unitMenu"><div class="MPos displayNone"><img width="15" height="15" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MPos.png"></div><span class="unitMenuTitle">' + myTitle + '</span><div class="menuXpand listHidden"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i></div></div>';

        /* HTML FOR NOT LEVELED Menu Items
        theHTML = '<li id="' + myID + '" class="menuEx" data-id="' + myID + '" data-actnumunit="' + acc.actnumunit + '" data-actnumwhole="' + acc.actnumwhole + '" ><div class="activity"><div class="MPos displayNone"><img width="15" height="15" src="'+acc.RESOURCES_FOLDER+'/layout/menu/MPos.png"></div><a class="activityTitle" href="#" >' + myTitle + '</a><div class="menuXpand listHidden"></div></div>';
        */

        if (Array.isArray(theObj["level" + nextLevel])) {


            theHTML += '<ul class="subMenu subMenu' + lvl + '">';
            //consoleMe("1: "+myID);

            /*
            if (Object.size(theObj["level" + nextLevel]) == 0) {
                theHTML = theHTML.replace('<img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXplus.png">', '');
            } else {
                for (var i = 0; i < Object.size(theObj["level" + nextLevel]); i++) {
                    theHTML += createEachLevel(nextLevel, theObj["level" + nextLevel][i], myID + "_" + i);
                }
            }
            */
            for (var i = 0; i < Object.size(theObj["level" + nextLevel]); i++) {
                theHTML += createEachLevel(nextLevel, theObj["level" + nextLevel][i], myID + "_" + i);
            }

            theHTML += '</li></ul>';
        } else {
            //consoleMe("2: "+myID);
            theHTML += '</li>';
            theHTML += createEachLevel(nextLevel, theObj["level" + nextLevel], myID + "_0");
        }


    } else {
            //consoleMe("3: "+myID);
            
        
            /****
            * Count & Save as Element attribute the Number of Activity per Unit or in the whole of tha app
            ****/
            acc.actnumunit++;
            acc.actnumwhole++;
        
            theHTML = '<li id="' + myID + '" class="menuEx" data-id="' + myID + '" data-actnumunit="' + acc.actnumunit + '" data-actnumwhole="' + acc.actnumwhole + '" ><div class="activity"><div class="MPos displayNone"><img width="15" height="15" src="'+acc.RESOURCES_FOLDER+'/layout/menu/MPos.png"></div><a class="activityTitle" href="#" >' + myTitle + '</a><div class="menuXpand listHidden"></div></div>';

            
    }


    /*
    if (theObj.hasOwnProperty("level" + nextLevel)) {
        
        consoleMe("nextLevel: "+nextLevel+" "+theObj.hasOwnProperty("level" + nextLevel))
        
        // Check Init Properties how deeep it should go
        if (nextLevel - 1 >= acc.lvlOfAccDevelopment) {
            hideLevelClass = ' alwaysHidden';
        }

        /* Classes For Main Menu
        theHTML = '<li data-id="' + myID + '" ><div class="MPos displayNone"><img width="15" height="15" src="'+acc.RESOURCES_FOLDER+'/layout/header/menu/MPos.png"></div><span>' + myTitle + '</span><div class="menuXpand listHidden'+hideLevelClass+'"><img width="16" height="16" src="'+acc.RESOURCES_FOLDER+'/layout/header/menu/MXplus.png"></div></li>';
        */

    /*
        theHTML = '<li data-id="' + myID + '" ><div class="MPos displayNone"><img width="15" height="15" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MPos.png"></div><span>' + myTitle + '</span></li>';


        //if (theObj["level"+nextLevel]){
        theHTML += '<ul class="accClass' + hideLevelClass + '">';
        if (Array.isArray(theObj["level" + nextLevel])) {
            if (Object.size(theObj["level" + nextLevel]) == 0) {
                theHTML = theHTML.replace('<img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXplus.png">', '');
            } else {
                for (var i = 0; i < Object.size(theObj["level" + nextLevel]); i++) {
                    theHTML += createEachLevel(nextLevel, theObj["level" + nextLevel][i], myID + "_" + i);
                }
            }
        } else {
            theHTML += createEachLevel(nextLevel, theObj["level" + nextLevel], myID + "_0");
        }
        theHTML += '</ul>';
        theHTML = theHTML.replace('<ul class="accClass"></ul>', '');
        
    } else { 
        
        
        
        /* Classes For Main Menu
        theHTML = '<li data-id="' + myID + '" ><div class="MPos displayNone"><img width="15" height="15" src="'+acc.RESOURCES_FOLDER+'/layout/header/menu/MPos.png"></div><span>' + myTitle + '</span><div class="menuXpand listHidden"></div></li>';
        */

    /*
        theHTML = '<li data-id="' + myID + '" ><div class="MPos displayNone"><img width="15" height="15" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MPos.png"></div><span>' + myTitle + '</span></li>';


        var a = 0;
        myID = myID + "";
        
        if (myTypeIs(myID + "") == "exercise") {
            if ((lvl + 1) < acc.DEPTH_COUNT) {
                acc.pageNo = 0;
            }
            var myI = myID.substr(myID.lastIndexOf("_") + 1);
            var extraClass = '';
            if (myID.lastIndexOf("_") >= 0) {
                extraClass = (myI % 2 == 0 ? '' : ' altColor');
            }
            acc.pageNo++;
            // Classes For Main Menu - Insert Text to Menu
            //theHTML = '<li data-id="' + myID + '" class="menuEx' + extraClass + '" data-page="' + acc.pageNo + '"><div class="MPos displayNone"><img width="15" height="15" src="'+acc.RESOURCES_FOLDER+'/layout/header/menu/MPos.png"></div><a href="#" >' + localizeText(theObj.title, acc.myLang) + '</a></li><ul></ul>';
            theHTML = '<li data-id="' + myID + '" data-toggle="tooltip" data-placement="top" title="' + localizeText(theObj.title, acc.myLang) + '" class="custom-tooltip menuEx' + extraClass + '" data-page="' + acc.pageNo + '"><div class="MPos displayNone"><img width="15" height="15" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MPos.png"></div><a href="#" ></a></li><ul></ul>';
        }
    }

*/

    //consoleMe("!createEachLevel- theHTML: " + theHTML)


    return theHTML;
}


/************************************************* 
            Menu Events 
*************************************************/

/*********************** 
        BURGER
************************/
function showBurger(e){
    // Show Opacity (to know if it is open/close)
    document.querySelector("nav").style.opacity = 1;

    // Show Basic Menu
    $('nav').show(100);
    //removeClass(document.querySelector("nav"), "lowerIndex");
    
    // Disactivate iFrame Events + Assign hideBurger() to Click on iFrame
    $('#activityIFrame').css('pointer-events', 'none');    
    document.getElementById('contentDIV').addEventListener("click", hideBurger);
}

function hideBurger(e){
    // Hide Opacity (to know if it is open/close)
    document.querySelector("nav").style.opacity = 0;

    // Hide All Menus
    
    $('nav .subMenu').hide(100);
    $('nav').hide(1000); 
  
    
    // Hide Menu Children
    var allLevelSubMenu = document.getElementsByClassName("listShown");
    if (allLevelSubMenu.length > 0) {
        //showHideChildren(allLevelSubMenu[i]);
        $('.unitMenu > .menuXpand').removeClass("listShown");
        $('.unitMenu > .menuXpand').addClass("listHidden");
        /*
        $('nav > ul span').css("border", "none"); 
        $('.unitMenu > .menuXpand').parent().next().slideUp(500);
        $('.unitMenu > .menuXpand').html('<i class="fa fa-caret-square-o-right" aria-hidden="true"></i>');
        */
    } else {
        showHideChildren(e);
    }
    
    
    // Activate iFrame Events Again + Deassign hideBurger() from iFrame
    $('#activityIFrame').css('pointer-events', 'auto');
    document.getElementById('contentDIV').removeEventListener("click", hideBurger);
    
}

/* EVENT for the Burger MEnu Button */
function assign_Burger_Menu_BTN() {

    /* Classes For Main Menu */
    // HIDE THE MENU - RUN ONCE INIT
    $("nav").css("opacity", 0);
    
    // LOWER THE MENU's INDEX (Hide)
    //$("nav").addClass("lowerIndex");

    // ADD MENU's POSITION CLASSES
    //$("nav").addClass("responSize");

    document.getElementById("burgerButton").style.cursor = 'pointer';


    // Classes For Main Menu - EVENT TO OPEN MENU
    document.getElementById("burgerButton").addEventListener("click", function (e) {
        consoleMe("** burgerButton CLICKED **")        

        if (window.getComputedStyle(document.querySelector("nav")).opacity == 0 || (window.getComputedStyle(document.querySelector("nav")).display == 'none' || window.getComputedStyle(document.querySelector("nav")).display == '')) {
            
            showBurger(e);
            
        } else {
            
            hideBurger(e);
            
        }
        
        // Hide Menu Children
        /*showHideChildren(e);*/
        
        e.stopPropagation();    
    });
}

/*
function level2ndClick() {
    consoleMe("!level2ndClick")
    
    var myLength;
    var myID = getDataAttribute(this, 'id');
    cleanSelections();
    if (false == $(this).next().is(':visible')) {
        /*$('nav > ul').slideUp(300);
    }	
	updateMenu(myID);
	document.getElementById("contentDIV").scrollTop=0;
    document.getElementById("contentDIV").style.overflow = "hidden";
    updateContent(myID);
}
*/

/* EVENT for all the Units’ .menuXpand - It closes Parents open Structure  */
function showHideChildren(e) {
    
    consoleMe("** showHideChildren **");

    $('.unitMenu > .menuXpand').not(this).parent().next().slideUp(100);
    $('.unitMenu > .menuXpand').not(this).removeClass("listShown");
    $('.unitMenu > .menuXpand').not(this).addClass("listHidden");
    $('.unitMenu > .menuXpand').not(this).html('<i class="fa fa-caret-square-o-right" aria-hidden="true"></i>');
    
    // $('.unitMenu > .menuXpand').html('<img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXplus.png">');
    //$('.unitMenu > .menuXpand').html('<i class="fa fa-caret-square-o-left" aria-hidden="true"></i>');
    
    consoleMe('hasClass(this, "listShown") 1')
    consoleMe(hasClass(this, "listShown"))
    
    if (hasClass(this, "listShown")) {
        consoleMe('>> listHIDE')

        $(this.parentNode).next().slideUp(300);
        $(this).removeClass("listShown");
        $(this).addClass("listHidden");
        //this.innerHTML = '<img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXplus.png">';
        //this.innerHTML = '<i class="fa fa-bullseye" aria-hidden="true"></i>';
        this.innerHTML = '<i class="fa fa-caret-square-o-right" aria-hidden="true"></i>';

        //$('#burgerButton').click();
        
        if (e.stopPropagation){
            e.stopPropagation();
        }
        
        
    } else {
        consoleMe('>> listSHOW')

        $(this.parentNode).next().slideDown(300);
        $(this).removeClass("listHidden");
        $(this).addClass("listShown");
        // this.innerHTML = '<img width="16" height="16" src="' + acc.RESOURCES_FOLDER + '/layout/header/menu/MXminus.png">';
        this.innerHTML = '<i class="fa fa-caret-square-o-left" aria-hidden="true"></i>';

        if (e.stopPropagation){
            e.stopPropagation();
        } 
    }

    consoleMe('hasClass(this, "listShown") 2')
    consoleMe(hasClass(this, "listShown"))
    
    //e.stopPropagation();    
}

/************************************************* 
            NAV MENU EVENTS
*************************************************/


/* EVENT for all the Units’ <li> of the Menu Structure */
function menuUnitsClick(e) {
    
    var clickExpandsSelection = acc.levelExpands;
    consoleMe("** menuUnitsClick **")

    if (!clickExpandsSelection) {

        var myLength;
        var myID = getDataAttribute(this, 'id');

        consoleMe("menuUnitsClick | acc.levelExpands FALSE -> myID: " + myID)

        cleanSelections();
        if (false == $(this).next().is(':visible')) {
            hideTheMenu();
            consoleMe("hideTheMenu")
        }
        updateMenu(myID);
        document.getElementById("contentDIV").scrollTop = 0;
        document.getElementById("contentDIV").style.overflow = "hidden";
        updateContent(myID);
        document.getElementById("goBack").disabled = false;
        document.getElementById("goBack").className = "uibuttons navButtonsEnabled";
        document.getElementById("goBack").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnUp_On.png";
    } else {
        consoleMe("menuUnitsClick | acc.levelExpands TRUE -> children .menuXpand -> showHideChildren")
        //$(this.lastChild).click();
        $(this).children('div').children('.menuXpand').click();
    }
    
    e.stopPropagation();
}


/* EVENT for all the Units -> Activities’ <li> of the Menu Structure */
function menuActivitiesClick() {
    consoleMe("** menuActivitiesClick **");

    var allmenuExs = document.getElementsByClassName("menuEx");
    if (allmenuExs.length > 0) {
        for (i = 0; i < allmenuExs.length; i++) {
            if (i > 4 && i < 14) {

                
                consoleMe("**** i *****  " + i)
                consoleMe(allmenuExs[i])


                allmenuExs[i].addEventListener("click", function (e) {

                        consoleMe("<<menuActivitiesClick addEventListener>>");

                        loadExercise(getDataAttribute(this, 'id'));

                        //loadExercise("end");

                        // Classes For Main Menu - MENU UPDATE
                        $(".custom-tooltip").tooltip("hide");
                        cleanSelections();


                        // Classes For Main Menu - Add Yellow Border ON Click - Change Background of VISITED button

                        var theAElement = this.querySelector("a");
                        theAElement.style.color = "#565549";
                        /*theAElement.style.backgroundColor = "#0b72b6";*/
                        insertBorder(theAElement);
                        
                        
                        /* ADD All Bookmark Classes on CLICK 
                        $(".menuEx > a > span" ).addClass('borderAnimationSpanAfterClick');
                        if($(this).find( "span" )){
                            $(this).find( "span" ).removeClass('borderAnimationSpanAfterClick');
                        }
                        */

                        updateMenu(getDataAttribute(this, 'id'));

                        // Reset Photo of Burger menu?
                        document.getElementById("burgerButton").firstChild.src = acc.RESOURCES_FOLDER + "/layout/header/menu-icon.png";
                        
                        //Hide tha navigation list....
                        hideBurger(e);

                        consoleMe("<<menuActivitiesClick getDataAttribute(this, 'id')>>")
                        consoleMe(getDataAttribute(this, 'id'))

                        // Check if the clicked Menu item is a submenu One
                        if ($(this.parentNode.parentNode).hasClass("hasNextLevel")) {
                            $('nav > ul span').css("border", "none");
                            var theParentElement = this.parentNode.parentNode.querySelector("span");
                            theParentElement.style.color = "#565549";
                            insertBorder(theParentElement);
                        } else {
                            /* 
                            $('nav .subMenu').hide(10);
                            */
                            $('nav > ul span').css("border", "none"); 
                            $('.unitMenu > .menuXpand').parent().next().slideUp(500);
                            $('.unitMenu > .menuXpand').removeClass("listShown");
                            $('.unitMenu > .menuXpand').addClass("listHidden");
                            $('.unitMenu > .menuXpand').html('<i class="fa fa-caret-square-o-right" aria-hidden="true"></i>');                       
                        }
                        
                        /* NEW - This is an OLD style of Closing the NAV                    
                        /* Classes For Main Menu
                        removeClass(document.querySelector("nav"), "fadeInLeft");
                        $("nav").addClass("fadeOutLeft animated", function () {

                        });
                        $("nav").addClass("lowerIndex");
                        */
                        
                    
                        /* New Disactivated
                        if (!hasClass(document.getElementById("goBack"), "displayNone")) {
                            document.getElementById("goBack").disabled = false;
                            document.getElementById("goBack").className = "uibuttons navButtonsEnabled";
                            document.getElementById("goBack").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnUp_On.png";
                        }
                        */
                        
                        /*
                        document.getElementById("goHome").disabled = false;
                        document.getElementById("goHome").className = "uibuttons navButtonsEnabled";
                        document.getElementById("goHome").firstChild.src = acc.RESOURCES_FOLDER+"/layout/navigation/BtnHome_On.png";
                        */
                        
                        // IMPORTANT
                        e.stopPropagation();
                    
                        //consoleMe("menuActivitiesClick")
                    
                    });
            }
        }
    }

}


function assign_All_Menu_Btns() {
    consoleMe("assign_All_Menu_Btns");

    /* Assign Burger Menu Button Event */
    if (acc.hiddenAccordion == true) {
        document.querySelector("#burgerButton").style.display = "none";
        document.querySelector("nav").style.display = "none";
    } else {
        assign_Burger_Menu_BTN();
    }


    /* Assign All Units Levels with Arrays [subCategories] [li] Event */
    $('.topMenuLevel > li.hasNextLevel').on('click', menuUnitsClick);  

    /* Assign All Units Expand Button  [div class="menuXpand] Event */
    $('.unitMenu .menuXpand').on('click', showHideChildren);

    /* Assign All Activities Button [li] Event */
    menuActivitiesClick();

}

/************************************************* 
                UPDATES
*************************************************/

function updateTitles(linkElem,howDeep,myLevel) {

    /* document.getElementById("theTitles_app").innerHTML = '<span>' + acc.menuObj.menu.title + '</span>'; */
    document.getElementById("theTitles_app").innerHTML = '<h1>' + acc.menuObj.menu.level1[linkElem[0]].title + '</h1>';
    
    consoleMe("**updateTitles - acc.menuObj:**")
    consoleMe(acc.menuObj)

    var str1=acc.menuObj.menu.level1[linkElem[0]];
    var str2;
    var str3 = "";

    consoleMe(linkElem)

    for (var i = 1; i < linkElem.length; i++) {
        
        str2=str1["level"+(i+1)][linkElem[i]];
        
        /* Include Activity Title 
        str3 = str3 + str1.title+"&emsp;>&emsp;";
        */
        str3 = str3 + str1.title;
        
        str1=str2;
    }
    
    /* Include Activity Title 
    str3 = str3 + str1.title;
    */
    str3 = str3;

    /* document.getElementById("theTitles_unit_group").innerHTML = '<span><h5>' + str3 + '</h5></span>'; */
    
    if(acc.menuObj.menu.level1[linkElem[0]].level2 != undefined){
        console.log(acc.menuObj.menu.level1[linkElem[0]].level2[linkElem[1]].title)
        document.getElementById("theTitles_unit_group").innerHTML = '<h2>' + acc.menuObj.menu.level1[linkElem[0]].level2[linkElem[1]].title + '</h2>';
    } else {
        console.log("OKKKKKK")
        document.getElementById("theTitles_unit_group").innerHTML = '<h2></h2>';
    }
    
    
    /*
    if (linkElem.length == 1) {
        consoleMe("1")
        var myTitlePart = localizeText(myPosition[linkElem[linkElem.length - 1]].title, acc.myLang).split("|");
        
        document.getElementById("theTitles_unit_group").innerHTML = '<span id="unitNum">' + myTitlePart[0] + '</span>';
       
        document.getElementById("theTitles_lesson_title").innerHTML = "";
        
    } else if (linkElem.length == 2) {
        
        var myTitlePart = localizeText(acc.menuObj.menu.level1[linkElem[0]].title, acc.myLang).split("|");
        
        var myLessonPart = localizeText(acc.menuObj.menu.level1[linkElem[0]].level2[linkElem[1]].title, acc.myLang);
        
        document.getElementById("theTitles_unit_group").innerHTML = '<span id="unitNum">' + myTitlePart[0] + '</span><span id="unitTitle">' + myLessonPart + '</span>';
        
        document.getElementById("theTitles_lesson_title").innerHTML = "";
        
    } else {
        
        var myTitlePart = localizeText(acc.menuObj.menu.level1[linkElem[0]].title, acc.myLang).split("|");

        var myLessonPart = localizeText(acc.menuObj.menu.level1[linkElem[0]].level2[linkElem[1]].title, acc.myLang);
        
        document.getElementById("theTitles_unit_group").innerHTML = '<span id="unitNum">' + myTitlePart[0] + '</span><span id="unitTitle">' + myLessonPart + '</span>'
        
        var myActivityPart = localizeText(acc.menuObj.menu.level1[linkElem[0]].level2[linkElem[1]].level3[linkElem[2]].title, acc.myLang);
        document.getElementById("theTitles_lesson_title").innerHTML = '<span>' + myActivityPart + '</span>';
    }
    */
}







function closeMenuOnFocus() {
    consoleMe("!closeMenuOnFocus")


    document.getElementById("burgerButton").firstChild.src = acc.RESOURCES_FOLDER + "/layout/header/menu-icon.png";
    //Hide tha navigation list....	

    /* Classes For Main Menu
	removeClass(document.querySelector("nav"), "fadeInLeft");
	$("nav").addClass("fadeOutLeft animated", function () {
	});
	$("nav").addClass("lowerIndex");
    */
}








/********************** MENU FUNCTIONALITY *************************/


function updateButtons(linkID) {
    consoleMe("! updateButtons")

    var linkElem = linkID.split("_");
    var exNum = linkElem[linkElem.length - 1];
    var myPosition;
    var myLevel;

    /* Inital or Final Page */
    if ((linkID != "home") && (linkID != "end")) {

        myPosition = acc.menuObj.menu;

        var lockPrevious = true;
        var lockNext = true;
        
        // New - Create Array to Keep Levels
        var howDeep = [];

        for (var i = 0; i < linkElem.length - 1; i++) {
            myLevel = "level" + (i * 1 + 1);
            if (linkElem[i] * 1 > 0) {
                lockPrevious = false;
            }
            if (Object.size(myPosition[myLevel]) - 1 != linkElem[i] * 1) {
                lockNext = false;
            }

            myPosition = myPosition[myLevel][linkElem[i]];
            
            
            howDeep.push(myLevel);
        }
        

        myLevel = "level" + (i * 1 + 1);
        myPosition = myPosition[myLevel];
        
        
        // New - Update the Top Titles:
        howDeep.push(myLevel);
        updateTitles(linkElem, howDeep);
        

        

        if (exNum > 0) {
            lockPrevious = false;
        }
        if (exNum < Object.size(myPosition) - 1) {
            lockNext = false;
        }

        if (myPosition[linkElem[i]].type == undefined) {
            lockPrevious = true;
            lockNext = true;
        }

        if (lockPrevious) {
            document.getElementById("previous").disabled = true;
            document.getElementById("previous").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnPrevious_Dis.png";
            document.getElementById("previous").style.display = "inline"
            $(document.getElementById("previous")).addClass("navButtonsDisabled");

            /* Classes For Main Menu 
            document.getElementById("previous").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom,  #f7f7f7 0%, #e1e1e1 50%, #cbcbcb 100%) repeat scroll 0 0";
            */
        } else {
            document.getElementById("previous").disabled = false;
            document.getElementById("previous").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnPrevious.png";
            $(document.getElementById("previous")).addClass("navButtonsEnabled");

            document.getElementById("previous").style.display = "inline"
            $(document.getElementById("previous")).removeClass("navButtonsDisabled");

            /* Classes For Main Menu 
            document.getElementById("previous").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom, #00e4d0 0%,#00a899 50%,#00544c 100%) repeat scroll 0 0";
            */
        }

        if (lockNext) {
            if (acc.completePage) {
                //document.getElementById("next").setAttribute("name","home");
                exNum == "home"
                document.getElementById("next").disabled = false;
                document.getElementById("next").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnNext.png";
                $(document.getElementById("next")).addClass("navButtonsEnabled");
                /* Classes For Main Menu 
				document.getElementById("next").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom, #00e4d0 0%,#00a899 50%,#00544c 100%) repeat scroll 0 0";
                */
                document.getElementById("next").style.display = "inline"
                $(document.getElementById("next")).removeClass("navButtonsDisabled");
            } else {
                document.getElementById("next").disabled = true;
                document.getElementById("next").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnNext_Dis.png";
                /* Classes For Main Menu 
                document.getElementById("next").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom, #8e837e 0%,#66605d 50%,#595451 100%) repeat scroll 0 0";
                */
                document.getElementById("next").style.display = "inline"
                $(document.getElementById("next")).addClass("navButtonsDisabled");
            }
            //consoleMe('eimai full '+Object.size(myPosition))
        } else {
            document.getElementById("next").disabled = false;
            document.getElementById("next").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnNext.png";
            $(document.getElementById("next")).addClass("navButtonsEnabled");
            /* Classes For Main Menu 
            document.getElementById("next").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom, #00e4d0 0%,#00a899 50%,#00544c 100%) repeat scroll 0 0";	
            */
            document.getElementById("next").style.display = "inline"
            $(document.getElementById("next")).removeClass("navButtonsDisabled");
        }

        /********
        * COUNTER 
        * Update the Counting of Activites Per UNIT
        ********/
        
        /* PER UNIT OLD 
        document.getElementById("counter").style.display = "inline";
        
        var theText = (exNum * 1 + 1) + " / " + Object.size(myPosition);
        document.getElementById("counter").value = theText;
        */
        
        /* PER UNIT NEW 
        document.getElementById("actnumwhole").style.display = "inline";
        
        var myPageNumber = getThePageNumber();
        document.getElementById("actnumwhole").value = myPageNumber[0]+ " / " +myPageNumber[1];       
        */
        
        /* PER APP CURRENT */
        document.getElementById("actnumwhole").style.display = "inline";
        
        var myPageNumber = getThePageNumber();
        document.getElementById("actnumwhole").value = myPageNumber[0]+ " / " +myPageNumber[2]; 
       
        // MANUAL SOLUTION
        if(exNum>4){
            document.getElementById("previous").disabled = true;
            document.getElementById("previous").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnPrevious_Dis.png";
            $(document.getElementById("previous")).addClass("navButtonsDisabled");
            document.getElementById("previous").style.display = "inline" 
        }

        
        /* New Disactivated
        if (!hasClass(document.getElementById("goBack"), "displayNone")) {
            if (linkElem.length <= 2) {
                if (acc.levelExpands == true) {
                    document.getElementById("goBack").disabled = true;
                    //removeClass(document.getElementById("goBack"), "navButtonsEnabled");
                    document.getElementById("goBack").className = "uibuttons navButtonsDisabled";
                    document.getElementById("goBack").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnUp_Dis.png";
                }
            } else {
                //
                document.getElementById("goBack").disabled = false;
                //removeClass(document.getElementById("goBack"), "navButtonsDisabled");
                document.getElementById("goBack").className = "uibuttons  navButtonsEnabled";
                document.getElementById("goBack").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnUp_On.png";
            }
        }
        */
        /*
        if (!hasClass(document.getElementById("goHome"), "displayNone")){
        	if (linkElem.length <= 2) {
        		if (acc.levelExpands==true){
        			document.getElementById("goHome").disabled = true;
        			//removeClass(document.getElementById("goHome"), "navButtonsEnabled");
        			document.getElementById("goHome").className = "uibuttons navButtonsDisabled";
        			document.getElementById("goHome").firstChild.src = acc.RESOURCES_FOLDER+"/layout/navigation/BtnHome_Dis.png";
        		}
        	}
        	else {
        		//
        		document.getElementById("goHome").disabled = false;
        		//removeClass(document.getElementById("goHome"), "navButtonsDisabled");
        		document.getElementById("goHome").className = "uibuttons  navButtonsEnabled";
        		document.getElementById("goHome").firstChild.src = acc.RESOURCES_FOLDER+"/layout/navigation/BtnHome_On.png";
        	}
        }
        */
    } else {
        document.getElementById("previous").disabled = true;
        document.getElementById("previous").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnPrevious_Dis.png";
        $(document.getElementById("previous")).addClass("navButtonsDisabled");
        document.getElementById("previous").style.display = "inline"
        document.getElementById("next").disabled = true;
        document.getElementById("next").firstChild.src = acc.RESOURCES_FOLDER + "/layout/navigation/BtnNext_Dis.png";
        document.getElementById("next").style.display = "inline"
        $(document.getElementById("next")).addClass("navButtonsDisabled");
        document.getElementById("counter").value = "";

        /* Classes For Main Menu 
        document.getElementById("previous").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom,  #f7f7f7 0%, #e1e1e1 50%, #cbcbcb 100%) repeat scroll 0 0";
        document.getElementById("next").style.background="rgba(0, 0, 0, 0) linear-gradient(to bottom,  #f7f7f7 0%, #e1e1e1 50%, #cbcbcb 100%) repeat scroll 0 0";
        */

        acc.unitState.isComplete = true;
        acc.unitState.grade = 100;
        complete(acc.unitState);
    }
}

function updateMenu(linkID) {
    consoleMe("** updateMenu **")

    var link_array;
    var temp_linkID;
    acc.linkID = linkID;

    if ((linkID != "home") && (linkID != "end")) {
        if ($("nav li[data-id=" + linkID + "]").css('display') == 'none') {
            $("nav li[data-id=" + linkID + "]").show();
        }
        $("nav li[data-id=" + linkID + "]").addClass("activeMenuElement");
        $("nav li[data-id=" + linkID + "]").next().slideDown(300);

        /* Classes For Main Menu 
        $("nav li[data-id=" + linkID + "] > .menuXpand").removeClass("listHidden");
        $("nav li[data-id=" + linkID + "] > .menuXpand").addClass("listShown");
        $("nav li[data-id=" + linkID + "] > .menuXpand").html($("nav li[data-id=" + linkID + "] > .menuXpand").html() == "" ? "" : '<img width="16" height="16" src="'+acc.RESOURCES_FOLDER+'/layout/header/menu/MXminus.png">');

        // SHOW MENU INDICATOR (Graphic MPos)
        //$("nav li[data-id=" + linkID + "] > .MPos").removeClass("displayNone");
        */

        if (acc.lvlOfAccDevelopment != "full") {
            link_array = linkID.split("_");
            temp_linkID = link_array[0];
            for (var i = 1; i < acc.lvlOfAccDevelopment; i++) {
                temp_linkID = temp_linkID + "_" + link_array[i];
            }
            $("nav li[data-id=" + temp_linkID + "] > .MPos").removeClass("displayNone");
        }
    } else {
        hideTheMenu();
        $('nav > ul > li').show();
    }

    
    

    //updateButtons(linkID);
    // Pass to audioPlayer the Current Activity ID
    myCurrentLinkID = linkID;


    try {
        consoleMe("THE ONLY USE OF main/breadcrumbs.js")
        updateBC(linkID);
    } catch (err) {
        //no crumbs
    }
}


function updateContent(linkID) {

    consoleMe("!updateContent");

    closeVideo();

    // RESET EXTRA BUTTONS
    resetExtraTextButtons();

    // AUDIO PLAYER
    disableAudioButtons();
    stopAudio();
    if (acc.TOOLBAR_EXE_ONLY) {
        disableToolbar();
    }
    $("#zoom-reset").click();
    var linkElem = linkID.split("_");
    var myPosition, myExtraElemPos, theHTML = "";
    var myLevel;
    var position;
    //consoleMe ('linkID = '+linkID+'  linkElem.length = '+linkElem.length)
    if ((linkID != "home") && (linkID != "end")) {
        //var mySTring = JSON.stringify(acc.menuObj)
        //consoleMe ('mySTring = '+mySTring)
        myPosition = acc.menuObj.menu;
        myExtraElemPos = acc.jsObjExtras.menu;
        for (var i = 0; i < linkElem.length; i++) {
            myPosition = myPosition["level" + (i + 1)][linkElem[i]];
            myExtraElemPos = myExtraElemPos["level" + (i + 1)][linkElem[i]];
        }

        if (myPosition.image) {
            /* NEW UNUSED
            document.querySelector("#selida").src = myPosition.image;
            document.querySelector("#selida").alt = localizeText(myPosition.title, acc.myLang);
            document.querySelector("#selida").className = "selida";
            
            if (acc.pageAppearance == "width") {
                //document.querySelector("#selida").style.width = "990px";
                document.getElementById("content-wrapper").style.overflow = "auto";
                if (!hasClass(document.querySelector("#selida"), "fullWidth")) {
                    document.querySelector("#selida").className = document.querySelector("#selida").className + " fullWidth";
                }
            } else {
                document.querySelector("#selida").style.height = "550px";
            }
            */
            if (!hasClass(document.querySelector("#exercise"), "hideMe")) {
                document.querySelector("#exercise").className = document.querySelector("#exercise").className + " hideMe";
            }
            /* NEW UNUSED
            if (!hasClass(document.querySelector("#selides"), "hideMe")) {
                document.querySelector("#selides").className = document.querySelector("#selides").className + " hideMe";
            }
            */
            myLevel = linkElem.length + 1;
            if (myPosition["level" + myLevel]) {

                if (Array.isArray(myPosition["level" + myLevel])) {

                    /* **********************
                     POSITION EXTRA KUKLAKIA
                     **************************/
                    //var myExtraElemPos = acc.menuObjExtras.menu["level" + (i + 1)][linkElem[i]];  
                    var allTogetherNow = Object.size(myExtraElemPos["level" + myLevel]) + Object.size(myPosition["level" + myLevel]);
                    for (f = 0; f < allTogetherNow; f++) {
                        if (myExtraElemPos["level" + myLevel][f]) {
                            //consoleMe("myExtraElemPos[level + myLevel][f].type: "+myExtraElemPos["level" + myLevel][f].type)							
                            if (myExtraElemPos["level" + myLevel][f].type === "video") {
                                //theHTML += ' <img src="'+acc.RESOURCES_FOLDER+'/layout/menu/videaki.png" alt="' + localizeText(myExtraElemPos["level" + myLevel][f].title, acc.myLang) + '" class="kyklakiaExtra" data-id="' + "extraElem" + linkID + '_' + f + '" style="left:' + (myExtraElemPos["level" + myLevel][f].x * 1) + 'px;top:' + (myExtraElemPos["level" + myLevel][f].y * 1) + 'px" />';
                                theHTML += ' <img src="' + acc.RESOURCES_FOLDER + '/layout/menu/videaki.png" alt="' + localizeText(myExtraElemPos["level" + myLevel][f].title, acc.myLang) + '" class="kyklakiaExtra" data-id="' + f + '@' + myExtraElemPos["level" + myLevel][f].type + '@' + myExtraElemPos['level' + myLevel][f].filename + '" style="left:' + (myExtraElemPos["level" + myLevel][f].x * 1) + 'px;top:' + (myExtraElemPos["level" + myLevel][f].y * 1) + 'px" />';
                            } else if (myExtraElemPos["level" + myLevel][f].type === "audio") {
                                theHTML += ' <img src="' + acc.RESOURCES_FOLDER + '/layout/menu/audiaki.png" alt="' + localizeText(myExtraElemPos["level" + myLevel][f].title, acc.myLang) + '" class="kyklakiaExtra" data-id="' + f + '@' + myExtraElemPos["level" + myLevel][f].type + '@' + myExtraElemPos['level' + myLevel][f].filename + '" style="left:' + (myExtraElemPos["level" + myLevel][f].x * 1) + 'px;top:' + (myExtraElemPos["level" + myLevel][f].y * 1) + 'px" />';
                            }
                        }
                    }

                    /* **********************
                     POSITION ACTIVITY KUKLAKIA
                     **************************/
                    for (i = 0; i < Object.size(myPosition["level" + myLevel]); i++) {

                        if (myPosition["level" + myLevel][i].x) {

                            if (myPosition["level" + myLevel][i].type === "exercise") {

                                var offsets = $('#contentDIV').offset();
                                var top = offsets.top - $('#contentDIV').height;
                                var left = offsets.left - $('#contentDIV').width;
                                position = $("#contentDIV").position();
                                //theHTML+=' <img src="layout/kyklaki2test.png" alt="'+myPosition["level"+myLevel][i].title[acc.myLang]+'" class="kyklaki" data-id="'+"kyklaki"+linkID+'_'+i+'" style="left:'+(position.left+myPosition["level"+myLevel][i].x*1)+'px;top:'+(position.top+myPosition["level"+myLevel][i].y*1)+'px" />'
                                //For simplicity's sake I omited the content position parameter in order to position  
                                //the kyklaki based on the position that the user sees on screen with Firebug.
                                //We will need it in the future, please don't delete above code yet. Thanks.
                                theHTML += ' <img src="' + acc.RESOURCES_FOLDER + '/layout/menu/kyklaki.png" alt="' + localizeText(myPosition["level" + myLevel][i].title, acc.myLang) + '" class="kyklaki" data-id="' + "kyklaki" + linkID + '_' + i + '" style="left:' + (myPosition["level" + myLevel][i].x * 1) + 'px;top:' + (myPosition["level" + myLevel][i].y * 1) + 'px" />';

                            } else {
                                // EXTRA INFO LINK!!!!
                                //consoleMe(myPosition["level" + myLevel][i].type)
                            }

                            /*POSITION KUKLAKI
                             else if (myPosition["level" + myLevel][i].type === "video") {
                             // VIDEO LINK!!!!
                             //consoleMe(myPosition["level" + myLevel][i].type);
                             theHTML += ' <img src="'+acc.RESOURCES_FOLDER+'/layout/menu/videaki.png" alt="' + localizeText(myPosition["level" + myLevel][i].title, acc.myLang) + '" class="kyklaki" data-id="' + "videaki" + linkID + '_' + i + '" style="left:' + (myPosition["level" + myLevel][i].x * 1) + 'px;top:' + (myPosition["level" + myLevel][i].y * 1) + 'px" />';
                             }
                             
                             */
                        }
                    }
                }
            }
            
            /* NEW UNUSED
            $("#selidaLinks").html(theHTML);
            
            

            document.querySelector("#selidaLinks").className = "";

            //
            if (document.getElementsByClassName("selida")[0].width > document.getElementsByClassName("selida")[0].height) {
                document.getElementsByClassName("selida")[0].className += " myWidth";
            } else {
                document.getElementsByClassName("selida")[0].className += " myHeight";
            }
            /* **********************
             ASSIGN EXTRA KUKLAKIA
             *************************
            var extrasKYklakia = document.getElementsByClassName("kyklakiaExtra");
            if (extrasKYklakia.length > 0) {
                for (i = 0; i < extrasKYklakia.length; i++) {
                    extrasKYklakia[i].addEventListener("click",
                        function () {
                            loadVideo(getDataAttribute(this, 'id'));
                        });
                }
            }
            /* **********************
             ASSIGN KUKLAKIA
             ************************
            var allKYklakis = document.getElementsByClassName("kyklaki");
            if (allKYklakis.length > 0) {
                for (i = 0; i < allKYklakis.length; i++) {
                    allKYklakis[i].addEventListener("click",
                        function () {
                            loadExercise(getDataAttribute(this, 'id'));
                            cleanSelections();
                            updateMenu(getDataAttribute(this, 'id').substr(7));
                        });
                }
            }
            
            */
            
        } else {
            myLevel = linkElem.length + 1;
            /* NEW UNUSED
            document.querySelector("#selida").src = "";
            document.querySelector("#selida").alt = "";
            document.querySelector("#selida").className = "selida hideMe";
            if (!hasClass(document.querySelector("#selidaLinks"), "hideMe")) {
                document.querySelector("#selidaLinks").className = document.querySelector("#selidaLinks").className + " hideMe";
            }
            */
            if (myPosition["level" + myLevel]) {
                if (!hasClass(document.querySelector("#exercise"), "hideMe")) {
                    document.querySelector("#exercise").className = document.querySelector("#exercise").className + " hideMe";
                }
                
                
                /* NEW UNUSED
                document.querySelector("#selides").style.visibility = "hidden";
                */
                
                createPages(linkID, myPosition, myLevel);
                var t = setTimeout(function () {
                    createPages(linkID, myPosition, myLevel);
                    document.querySelector("#selides").style.visibility = "visible";
                }, 500);
            }
        }
    } else if (linkID == "end") {
        document.getElementById("counter").value = "";
        var tempLang = (acc.myLang == acc.DEFLANG ? '' : '-' + acc.myLang);
        theHTML = '<iframe id="activityIFrame" name="activityIFrame" allowfullscreen src="' + acc.ACTIVITIES_FOLDER + '/activityEnd' + tempLang + '.html"></iframe>';
        $("#myObject").html(theHTML);
        if (!hasClass(document.querySelector("#selides"), "hideMe")) {
            document.querySelector("#selides").className = document.querySelector("#selides").className + " hideMe";
        }
    } else {
        document.getElementById("counter").value = "";
        var tempLang = (acc.myLang == acc.DEFLANG ? '' : '-' + acc.myLang);
        theHTML = '<iframe id="activityIFrame" name="activityIFrame" allowfullscreen src="' + acc.ACTIVITIES_FOLDER + '/activity' + tempLang + '.html"></iframe>';
        $("#myObject").html(theHTML);
        //$("#myObject").css("margin-top", "-550px");
        if (!hasClass(document.querySelector("#selides"), "hideMe")) {
            document.querySelector("#selides").className = document.querySelector("#selides").className + " hideMe";
        }
    }
}
