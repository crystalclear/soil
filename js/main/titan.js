function alertMe(msg){
    alert(msg);
}

function consoleMe(msg){
    console.log(msg);
}

var globalScore = 0;
function scoreTotal(score){
    if(score >= 100){
        globalScore +=1;
    }
    /* alert(globalScore); */
}

/* Titan Functions All Commented as Classes For Main Menu */

// Classes For Main Menu - New Function For Bookmark
function showBookMark(theText){
    //consoleMe("showBookMark: "+document.querySelector("li[data-id='" + acc.linkID + "']"));
    
    /* Classes For Main Menu - Use this if BookMark is for the relevant activity*/
    var currentElement = document.querySelector("li[data-id='" + acc.linkID + "'] > a");
    
    // ΚΑΡΦΩΤΟ
    var element = document.querySelector("li[data-id='0_2'] > a");

    // Clear Bookmark Text
    element.innerHTML='';
    // Insert a border independently if it is the current Bookmark
    insertBorder(element);
    // Trigger Animation
	element.classList.add("borderAnimation");
    
    element.addEventListener("animationend", function() {
        
        // Remove a border independently if it is the current Bookmark
        removeBorder(element, currentElement);
        // Remove Animation
        element.classList.remove("borderAnimation");
        // Insert  Bookmark Text
        element.innerHTML = '<span>'+theText+'</span>';
        
        // Fix Span/Text Position (without a border)
        var elementSpan = element.querySelector('span');
        elementSpan.classList.add("borderAnimationAdjust");
        
    }, false);
    
}