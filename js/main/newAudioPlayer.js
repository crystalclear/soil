function consoleMeAudioPlayer(msg) {
    console.log(msg);
}


// Path for Audio Player's Graphics
var audioPlayResourcesPath = acc.RESOURCES_FOLDER + '/layout/audioplayer/'


var audioPlayer = function (exerciseRef) {

    this.exerciseRef = exerciseRef;
    // Keep Data of Sprites/Songs JSON
    this.dataJSON = exerciseRef.activity.timelineObject;

    //alert(acc.state)
    // Keep the Audio File
    //this.audioFilename = acc.RESOURCES_FOLDER+'/audio/'+acc.state;

    // Playhead
    //let onplayheadMouseDown = false;
};


audioPlayer.prototype = {
    // ON Load APP / Main
    events_1StTime: function () {

        var self = this;

        //var that = this;

        // The top Window (if player is called within an iFrame)
        //this.theParentWindow = (window.parent) ? window.parent : window;
        this.theParentWindow = window;

        // Bind MoveHead MouseMove Event into a Variable;
        this.bindMeWithThis = this.moveplayhead.bind(this);

        // Cache references to DOM elements.
        var elmsGlobal = ['audioRewindButton', 'audioPlayButton', 'audioPauseButton', 'goToEnd', 'audioStopButton', 'audioForwardButton', 'timeline', 'playhead', 'activityIFrame'];
        // Keep All Player Elements
        elmsGlobal.forEach(function (elm) {
            //window[elm] = window.parent.document.getElementById(elm);
            window[elm] = document.getElementById(elm);
        });
        
        /*
        consoleMeAudioPlayer("acc.currentActFolder: " + acc.currentActFolder)
        consoleMeAudioPlayer("acc.state: " + acc.state)
        consoleMeAudioPlayer(acc.unitState)
        consoleMeAudioPlayer("acc.menuObj: " + acc.menuObj)
        consoleMeAudioPlayer("acc.RESOURCES_FOLDER: " + acc.RESOURCES_FOLDER)
        consoleMeAudioPlayer("acc.ACTIVITIES_FOLDER: " + acc.ACTIVITIES_FOLDER)
        */
        
        /*******
         * EVENTS
         ********/

        // Assign Audio Playhead / Slider
        playhead.addEventListener('mousedown', this.mouseDown.bind(this), false);
        //Makes timeline clickable
        timeline.addEventListener('click', this.clickTimeline.bind(this), false);

        this.theParentWindow.addEventListener('mouseup', this.mouseUp.bind(this), false);

        // Bind our player controls.
        audioPlayButton.addEventListener('click', function () {
            self.play();
        });
        audioStopButton.addEventListener('click', function () {
            self.stop();
            //self.onStopEnd();
        });
        goToEnd.addEventListener('click', function () {
            self.onStopEnd();
        });
        audioPauseButton.addEventListener('click', function () {
            self.pause();
        });
        audioRewindButton.addEventListener('click', function () {
            self.skipSprite('prev');
        });
        audioForwardButton.addEventListener('click', function () {
            self.skipSprite('next');
        });

    },
    /**
     * Sort the dataJSON SPrite Information According to self.dataJSON[i].start;
     */
    sortTheData: function () {

        var self = this;

        var compareNumbers = function (a, b) {
            return a[2] - b[2];
        }

        var arrayToSortSprites_Timing = [];

        // In case an Activity Does not have Sprites
        if (self.allSprites) {

            for (let a = 0; a < self.allSprites.length; a++) {
                let tempArray = [];
                tempArray[0] = self.allSprites[a].id;
                tempArray[1] = a;
                tempArray[2] = self.dataJSON[a].start;

                arrayToSortSprites_Timing.push(tempArray);

            }

            // Important: Sort It
            arrayToSortSprites_Timing.sort(compareNumbers)
            

            self.arrayToKeepSprites = [];
            self.arrayToKeepTiming = [];

            for (let b = 0; b < arrayToSortSprites_Timing.length; b++) {

                // Start from the Beggining
                if (b == 0) {

                    let tempArray = [];
                    tempArray.push(arrayToSortSprites_Timing[b][1]);

                    self.arrayToKeepSprites.push(tempArray);
                    self.arrayToKeepTiming.push(arrayToSortSprites_Timing[b][2]);

                } else if (arrayToSortSprites_Timing[b - 1][2] === arrayToSortSprites_Timing[b][2]) {


                    self.arrayToKeepSprites[(self.arrayToKeepSprites.length - 1)].push(arrayToSortSprites_Timing[b][1]);


                } else if (arrayToSortSprites_Timing[b - 1][2] <= arrayToSortSprites_Timing[b][2]) {
                    let tempArray = [];
                    tempArray.push(arrayToSortSprites_Timing[b][1]);

                    self.arrayToKeepSprites.push(tempArray);
                    self.arrayToKeepTiming.push(arrayToSortSprites_Timing[b][2]);

                } //End IF
                
                
                /* NEW II - GROUPS */
                if(self.allSprites[b].className.includes('groupParent')){
                    // Hide All the Groups
                    self.allSprites[b].style.opacity = 0;
                    // Count The number of Groups
                    self.groupTotal++;
                    
                    console.log(">>>>> self.groupTotal: "+self.groupTotal+" <<<<<<<");
                }
                

            } //End FOR



        } //End IF


        /****
        #Running functions before comparing values: https://css-tricks.com/level-up-your-sort-game/ 
        Sort the dataJSON by Start Value

        const sortByMapped = (map,compareFn) => (a,b) => compareFn(map(a),map(b));
        const byValue = (a,b) => a - b;
        const toStart = e => e.start;
        const byPrice = sortByMapped(toStart,byValue);
        consoleMeAudioPlayer(self.dataJSON.sort(byPrice));          
        ****/
        
    },

    /**
     * Reset a song in the dataJSON.
     * @param  {Number} index Index of the song in the dataJSON (leave empty to play the first or current).
     */
    initCurrentPlayer: function (allSprites, index, spriteNum) {

        var self = this;
        var sound;
        
        self.groupIndex = -1;
        self.groupTotal = -1;

        // Used for Different Songs - If Index is specified as number then stick with it, otherwise keep internal index
        //index = typeof index === 'number' ? index : self.index;
        index = typeof index === 'number' ? index : 0;
        // Keep track of the index we are currently playing.
        self.index = index;

        // Used for different parts of one song - If spriteNum is specified as number then stick with it, otherwise keep internal spriteNum
        //spriteNum = typeof spriteNum === 'number' ? spriteNum : self.spriteNum;
        spriteNum = typeof spriteNum === 'number' ? spriteNum : -1;
        // Keep track of the spriteNum we are currently playing.
        // Reset it to -1 to catch the 1st Transition
        self.spriteNum = spriteNum;

        // Playhead has a MouseDown Event?
        self.onplayheadMouseDown = false;

        // Keep Activity's Divs
        self.allSprites = allSprites;

        // Init Player Buttons
        this.showPlay();
        this.disactivateButs();

        // Activate Events in the Timeline
        self.activateTimeline();


        // Keep current JSON + its Data / If not Set an Empty Object in order to keep the index / song
        if (!self.dataJSON) {
            self.dataJSON = [{}];

            consoleMeAudioPlayer("INIT 1a >>> self.dataJSON = NULL");

        } else {
            consoleMeAudioPlayer("INIT 1b >>> self.dataJSON not NULL (Activity has Audio)");
            
        } // End IF

        // Save the Timing Data in Arrays
        self.sortTheData();

        var data = self.dataJSON[index];

        // If we already loaded this track (it is not null), use the current one.
        // Otherwise, setup and load a new Howl.
        if (data.howl) {
            sound = data.howl;

            consoleMeAudioPlayer("INIT 2a >>> ALREADY LOADED - sound = data.howl = " + data.howl)

        } else {
            
            consoleMeAudioPlayer("INIT 2b >>> NEW LOADING - sound = data.howl = " + data.howl+" | self.audioFilename: " + self.audioFilename)

            sound = data.howl = new Howl({
                /* src: ['./audio/' + data.file + '.webm', './audio/' + data.file + '.mp3'],*/
                src: [self.audioFilename + '.ogg', self.audioFilename + '.mp3'],
                html5: false, // Force to HTML5 so that the audio can stream in (best for large files).
                loop: false,
                autoplay: true,
                onplay: function () {

                    // Show the Pause Button + Activate the Other Buttons
                    self.showPause();
                    self.activateButs();

                    // Check if Sync Data Exists and if not Disactivate Prev/Next
                    if (self.dataJSON.length <= 1) {
                        self.disactivateRewind();
                        self.disactivateForward();
                    }

                    // Show Activity's Content
                    //self.handleSprites();

                    // Start upating the progress of the track.
                    requestAnimationFrame(self.step.bind(self));

                },
                onload: function () {
                    // Wait Audio to Load and then Handle Navigation Buttons
                    updateButtons(myCurrentLinkID);
                },
                onpause: function () {
                    // Show the play button.
                    self.showPlay();
                },
                onend: function () {
                    // Stop Sound
                    self.handleEnd();
                },
                onstop: function () {
                    self.handleStop();
                },
                onseek: function () {
                    self.showPause();

                    // Start upating the progress of the track.
                    requestAnimationFrame(self.step.bind(self));

                    // Update Sprites Timeline
                    console.log("onseek > *** handleSprites 1 ***")
                    self.handleSprites();
                    
                }
            });
        }

        // Already Loaded
        if (sound.state() === 'loaded') {}

    },

    unload: function () {

        var self = this;

        if (self.dataJSON[self.index].howl) {

            // Get the Howl we want to manipulate.
            var sound = self.dataJSON[self.index].howl;

            // -- Destroy the player with internal method
            //sound.unload(); 
            // -- Reference it to null
            sound = null;
            // -- Then delete the variable
            delete sound;
        }
    },

    showPlay: function () {
        audioPauseButton.style.display = 'none';

        audioPlayButton.style.display = 'inline-block';
        audioPlayButton.querySelector('img').src = audioPlayResourcesPath + 'BtnPlay.png';
        audioPlayButton.style.cursor = 'pointer';
        audioPlayButton.disabled = false;
    },

    showPause: function () {
        audioPlayButton.style.display = 'none';

        audioPauseButton.style.display = 'inline-block';
        audioPauseButton.style.cursor = 'pointer';
        audioPauseButton.disabled = false;

    },

    activateButs: function () {
        audioRewindButton.disabled = false;
        audioRewindButton.querySelector('img').src = audioPlayResourcesPath + 'BtnRewind.png';
        audioRewindButton.style.cursor = 'pointer';
        
        goToEnd.disabled = false;
        goToEnd.querySelector('img').src = audioPlayResourcesPath + 'BtnEnd.png';
        goToEnd.style.cursor = 'pointer';
        
        audioStopButton.disabled = false;
        audioStopButton.querySelector('img').src = audioPlayResourcesPath + 'BtnStop.png';
        audioStopButton.style.cursor = 'pointer';

        audioForwardButton.disabled = false;
        audioForwardButton.querySelector('img').src = audioPlayResourcesPath + 'BtnForward.png';
        audioForwardButton.style.cursor = 'pointer';

        playhead.classList.remove("disabled");
        timeline.classList.remove("disabled");

    },

    activateTimeline: function () {
        $(timeline).css('pointer-events', 'auto');
        timeline.classList.remove("disabled");

        durationIndicator.style.display = 'inline-block';
    },

    totalDisactivate: function () {
        var self = this;

        self.disactivateButs();
        audioPauseButton.style.display = 'none';

        audioPlayButton.disabled = true;
        audioPlayButton.querySelector('img').src = audioPlayResourcesPath + 'BtnPlay_Dis.png';
        audioPlayButton.style.cursor = 'default';

        $(timeline).css('pointer-events', 'none');

        durationIndicator.style.display = 'none';
    },

    disactivateButs: function () {
        audioRewindButton.disabled = true;
        audioRewindButton.querySelector('img').src = audioPlayResourcesPath + 'BtnRewind_Dis.png';
        audioRewindButton.style.cursor = 'default';

        goToEnd.disabled = true;
        goToEnd.querySelector('img').src = audioPlayResourcesPath + 'BtnEnd_Dis.png';
        goToEnd.style.cursor = 'default';
        
        audioStopButton.disabled = true;
        audioStopButton.querySelector('img').src = audioPlayResourcesPath + 'BtnStop_Dis.png';
        audioStopButton.style.cursor = 'default';

        audioForwardButton.disabled = true;
        audioForwardButton.querySelector('img').src = audioPlayResourcesPath + 'BtnForward_Dis.png';
        audioForwardButton.style.cursor = 'default';

        playhead.classList.add("disabled");
        timeline.classList.add("disabled");
    },
    disactivateEnd: function () {
        goToEnd.disabled = true;
        goToEnd.querySelector('img').src = audioPlayResourcesPath + 'BtnEnd_Dis.png';
        goToEnd.style.cursor = 'default';
    },
    disactivateStop: function () {
        audioStopButton.disabled = true;
        audioStopButton.querySelector('img').src = audioPlayResourcesPath + 'BtnStop_Dis.png';
        audioStopButton.style.cursor = 'default';
    },
    disactivateRewind: function () {
        audioRewindButton.disabled = true;
        audioRewindButton.querySelector('img').src = audioPlayResourcesPath + 'BtnRewind_Dis.png';
        audioRewindButton.style.cursor = 'default';
    },
    disactivateForward: function () {
        audioForwardButton.disabled = true;
        audioForwardButton.querySelector('img').src = audioPlayResourcesPath + 'BtnForward_Dis.png';
        audioForwardButton.style.cursor = 'default';
    },

    /**
     * Play a song in the dataJSON.
     * @param  {Number} index Index of the song in the dataJSON (leave empty to play the first or current).
     */
    play: function () {

        var self = this;

        if (self.dataJSON[self.index].howl) {

            // Get the Howl we want to manipulate.
            var sound = self.dataJSON[self.index].howl;

            // Begin playing the sound.
            sound.play();
        }
    },

    /**
     * Pause the currently playing track.
     */
    pause: function () {

        var self = this;

        if (self.dataJSON[self.index].howl) {
            // Get the Howl we want to manipulate.
            var sound = self.dataJSON[self.index].howl;
            // Pause the sound.
            sound.pause();
        }

    },

    /**
     * Stop the currently playing track.
     */
    stop: function () {

        var self = this;

        if (self.dataJSON[self.index].howl) {
            // Get the Howl we want to manipulate.
            var sound = self.dataJSON[self.index].howl;
            // Stop the sound.
            sound.stop();
        }


    },
    /**
     * Called on goToEnd
     */
    onStopEnd: function () {

        var self = this;

        if (self.dataJSON[self.index].howl) {
            // Get the Howl we want to manipulate.
            var sound = self.dataJSON[self.index].howl;

            if (!sound.playing()) {
                sound.play();
            }

            // Just b4 the end of Sound
            sound.seek(sound.duration() - 0.1);

        }
    },

    /**
     * Called onStop
     */
    handleStop: function () {

        var self = this;

        // Show the play button.
        self.showPlay();
        
        // Disactivate End Button
        //self.disactivateEnd();        
        // Disactivate Stop Button
        self.disactivateStop();
        
        // Reset Playhead
        playhead.style.marginLeft = 0 + "px";
        
        // Update Sprites Timeline
        self.hideSprites();
        
        
        
        /* NEW II - GROUPS */
        if(self.groupTotal > -1){
            // Reset GroupIndex
            self.groupIndex = -1;
            
            for (let a = 0; a < self.allSprites.length; a++) {
                if(self.allSprites[a].className != undefined && self.allSprites[a].className.includes('groupParent')){
                    
                    // Hide All Parent Sprites
                    self.allSprites[a].style.display = 'none';
                    
                    var pos = self.allSprites[a].className.lastIndexOf("groups")+6;
                    var res = self.allSprites[a].className.substr(pos, 1);  
                    
                    // Show Only 1st Parent Sprite
                    if(res == 0){
                        self.allSprites[a].style.display = 'block';
                    }
                }
            }
        }
        /* NEW II - GROUPS */
        
        
        
    },

    /**
     * Called onEnd
     */
    handleEnd: function () {

        var self = this;

        // Show the play button.
        self.showPlay();
        // Disactivate End Button
        self.disactivateEnd();
        // Disactivate Stop Button
        self.disactivateStop();
        // Disactivate Forward Button
        self.disactivateForward();

        // Shows Possible Extra Activity
        if (xtraActivityExists) {
            extraActivityShow();
        }
        
        
        /* NEW II - GROUPS */
        if(self.groupTotal > -1){
            // Reset GroupIndex
            //self.groupIndex = -1;
            
            for (let a = 0; a < self.allSprites.length; a++) {
                if(self.allSprites[a].className != undefined && self.allSprites[a].className.includes('groupParent')){
                    
                    // Progress Group Index
                    self.groupIndex++;
                    
                    // Hide All Parent Sprites
                    self.allSprites[a].style.display = 'none';
                    
                    var pos = self.allSprites[a].className.lastIndexOf("groups")+6;
                    var res = self.allSprites[a].className.substr(pos, 1);  
                    
                    // Show Only 1st Parent Sprite
                    if(res == self.groupTotal){
                        self.allSprites[a].style.display = 'block';
                    }
                    
                    
                }
            }
        }
        /* NEW II - GROUPS */
        
        
        
    },

    /**
     * Seek to a new position in the currently playing track.
     * @param  {Number} per Percentage through the song to skip.
     */
    doSeek: function (per) {

        var self = this;
        if (self.dataJSON[self.index].howl) {
            // Get the Howl we want to manipulate.
            var sound = self.dataJSON[self.index].howl;

            sound.seek(sound.duration() * per);

            // Convert the percent into a seek position.
            if (!sound.playing()) {
                sound.play();
            }
        }


    },


    /**
     * Skip to the next or previous Sprite.
     * @param  {String} direction 'next' or 'prev'.
     */
    skipSprite: function (direction) {
        var self = this;

        // Get the Howl we want to manipulate.
        var sound = self.dataJSON[self.index].howl;



        // Get the next sprite based on the direction of the track.
        //var spriteIndex = 0;
        if (direction === 'prev') {
            self.spriteNum = self.spriteNum - 1;
            // End of Previous
            if (self.spriteNum < 0) {
                /* Looping around
                spriteIndex = self.arrayToKeepTiming.length - 1;
                */
                self.spriteNum = 0;
            }

            // Activate Stop
            //self.activateButs();
            // Seek New Sprite
            sound.seek(self.arrayToKeepTiming[self.spriteNum]);

        } else {
            // Move to the Next Part
            self.spriteNum = self.spriteNum + 1;
            // End of Next
            if (self.spriteNum >= self.arrayToKeepTiming.length) {
                /* looping Around
                spriteIndex = 0;
                */
                sound.seek(sound.duration() - 0.1);

            } else {
                // Seek New Sprite
                sound.seek(self.arrayToKeepTiming[self.spriteNum]);
            }


        }

        if (!sound.playing()) {
            self.play();
        }

        // Update Current Sprite
        //self.spriteNum = spriteIndex;

        // Update Sprites Timeline
        //self.handleSprites();

    },

    /***
    The step called within requestAnimationFrame to update the playback position.
    */

    step: function () {
        var self = this;

        // Get the Howl we want to manipulate.
        var sound = self.dataJSON[self.index].howl;

        // Determine our current seekPosition.
        var seekPosition = sound.seek() || 0;

        // If the sound is still playing, continue stepping.
        if (sound.playing()) {


            // Make sure the SpriteNum is Reset on Begin Audio 
            if (seekPosition < 0.1) {
                self.spriteNum = -1;
                console.log("step > *** handleSprites 2 *** disactivated")
                
                
                
                
                /******************************************************************************
                *******************************************************************************
                HAS TO GET REMOVED --- BUT WHEN REMOVED ON INIT PLAYER DOESN" HIDE SPRITES 
                *******************************************************************************
                *******************************************************************************/
                self.handleSprites();
            }

            // Check If NEXT
            if (self.arrayToKeepTiming[self.spriteNum + 1]) {
                if (seekPosition > self.arrayToKeepTiming[self.spriteNum + 1]) {
                    
                    consoleMeAudioPlayer("step > if self.spriteNum + 1 > *** handleSprites 3 ***")
                    self.handleSprites();
                }
            }

            //playhead.style.left = (((seekPosition / sound.duration()) * 100) || 0) + '%';
            playhead.style.marginLeft = (((seekPosition / sound.duration()) * 100) || 0) + '%';


            // Display the progress / duration.
            durationIndicator.innerHTML = self.formatTime(Math.round(seekPosition)) + " / " + self.formatTime(Math.round(sound.duration()));
            //progress.style.width = (((seekPosition / sound.duration()) * 100) || 0) + '%';

            requestAnimationFrame(self.step.bind(self));
        }
    },

    /***
     * Function to Hide Timeline's Sprites
     */
    hideSprites: function () {

        var self = this;

        // Reset Current Sprite
        self.spriteNum = -1;

        for (var i = self.allSprites.length - 1; i >= 0; i--) {
            if(self.allSprites[i].className.includes('opacity_0')){
                self.allSprites[i].style.opacity = 0;
            } else {
                self.allSprites[i].style.opacity = this.myOpacity;
            }
            self.allSprites[i].classList.remove("fadeIn");
        } //End FOR

    },

    /***
     * Function to Handle the Timeline
     */
    handleSprites: function () {
        var self = this;

        // Get the Howl we want to manipulate.
        var sound = self.dataJSON[self.index].howl;

        // Determine our current seekPosition.
        var seekPosition = sound.seek() || 0;


        // Find All Sprites
        //var allSprites = document.querySelectorAll('.timeline-sprite');


        // If Seek is ahead of SpriteNum Timing Proceed to the next Sprite
        for (let b = self.arrayToKeepTiming.length - 1; b >= 0; b--) {

            consoleMeAudioPlayer("----- MAIN LOOP BEGINS - FOR > b: " + b+" -----");
            
            /***** 
             * Check if Next Sprite DOESN'T Exist && seekPosition is LARGER than CURRENT Sprite Timing
             * OR 
             * Check if Next Sprite EXISTS && seekPosition is larger than CURRENT Sprite Timing && seekPosition is smaller than NEXT Sprite Timing
             *****/
            if ((!self.arrayToKeepTiming[b + 1] && seekPosition >= self.arrayToKeepTiming[b]) || (self.arrayToKeepTiming[b + 1] && seekPosition >= self.arrayToKeepTiming[b] && seekPosition <= self.arrayToKeepTiming[b + 1])) {
                
                self.spriteNum = b;
                
                for (let c = 0; c < self.arrayToKeepSprites[b].length; c++) {
                    
                    let currentSprite = self.arrayToKeepSprites[b][c];
                    
                    consoleMeAudioPlayer("PLAYER NEXT 1: "+currentSprite);
                    
                    /* NEW II - GROUPS */
                    if(self.allSprites[currentSprite].className.includes('groupParent') || self.allSprites[currentSprite].className.includes('groupChild')){

                        self.allSprites[currentSprite].style.display = 'block';
                        
                        var myParent = self.allSprites[currentSprite].parentElement;
                        if(myParent.className.includes('groupParent')){
                           myParent.style.display = 'block';
                        }                        
                        
                        var pos = self.allSprites[currentSprite].className.lastIndexOf("groups")+6;
                        var res = self.allSprites[currentSprite].className.substr(pos, 1);
                        
                        //console.log("PLAYER NEXT 1: Handles NEXT SPRITES | self.groupIndex: "+self.groupIndex+" | self.allSprites[currentSprite].className: "+self.allSprites[currentSprite].className)
                        
                        self.groupIndex = res;
                        
                    }
                    
                    self.allSprites[currentSprite].style.opacity = 1;
                    
                    /* NEW II - Select if you want to add Fade */
                    if(self.allSprites[currentSprite].className.includes('addFadeIn')){
                        self.allSprites[currentSprite].classList.add('fadeIn');
                    }
                    

                } //End 2nd FOR    

                
            /***** 
            * Handles Next Seeking
            *****/
            } else if (seekPosition < self.arrayToKeepTiming[b]) {

                for (let stepUp = self.arrayToKeepSprites[b].length - 1; stepUp >= 0; stepUp--) {

                    let currentSprite = self.arrayToKeepSprites[b][stepUp];
                    
                    consoleMeAudioPlayer("PLAYER NEXT 2: "+currentSprite);
                    
                    if(self.allSprites[currentSprite].className.includes('opacity_0')){
                        self.allSprites[currentSprite].style.opacity = 0;
                        
                        console.log("newAudioPlayer.js > line 840 > Resets the syncClick Activity")
                        self.resetFromAudioPlayer();
                    
                    /* NEW II - GROUPS */
                    } else if(self.allSprites[currentSprite].className.includes('groupParent')){
                       
                        //console.log("PLAYER NEXT 2: Handles NEXT SPRITES | self.groupIndex: "+self.groupIndex+" | res: "+res+" | self.allSprites[currentSprite].className: "+self.allSprites[currentSprite].className)
                        
                        self.allSprites[currentSprite].style.display = 'none';
                        
                    } else {
                        self.allSprites[currentSprite].style.opacity = this.myOpacity;
                    }
                    
                    self.allSprites[currentSprite].classList.remove("fadeIn");
 
                }

            /***** 
            * Handles Previous Seeking if HIDE Object is selected in settings 
            *****/
            } else if (self.exerciseRef.settings.hideObject && seekPosition > self.arrayToKeepTiming[b]) {                
                
                for (let stepDown = 0; stepDown < self.arrayToKeepSprites[b].length; stepDown++) {

                    let currentSprite = self.arrayToKeepSprites[b][stepDown];
                    
                    consoleMeAudioPlayer("PLAYER PREVIOUS 1: "+currentSprite);
                    
                    if(self.allSprites[currentSprite].className.includes('opacity_0')){
                        self.allSprites[currentSprite].style.opacity = 0;
                    } else {
                        self.allSprites[currentSprite].style.opacity = this.myOpacity;
                    }
                    
                    
                    self.allSprites[currentSprite].classList.remove("fadeIn");
                    
                }

            /***** 
            * Handles Previous Seeking if HIDE Object is NOT selected in settings 
            *****/
            } else if (!self.exerciseRef.settings.hideObject && seekPosition > self.arrayToKeepTiming[b]) {

                for (let stepDownNoHide = 0; stepDownNoHide < self.arrayToKeepSprites[b].length; stepDownNoHide++) {

                    let currentSprite = self.arrayToKeepSprites[b][stepDownNoHide];
                    
                    consoleMeAudioPlayer("PLAYER PREVIOUS 2: "+currentSprite);
                   
                    /* NEW II - GROUPS */
                    if(self.allSprites[currentSprite].className.includes('groupParent')){
  
                        var pos = self.allSprites[currentSprite].className.lastIndexOf("groups")+6;
                        var res = self.allSprites[currentSprite].className.substr(pos, 1);                        
                    
                        if(res < self.groupIndex){
                            self.allSprites[currentSprite].style.display = 'none';
                            
                            //console.log("PLAYER PREVIOUS 2: Handles PREVIOUS SPRITES | self.groupIndex: "+self.groupIndex+" | res: "+res+" | self.allSprites[currentSprite].className: "+self.allSprites[currentSprite].className)
                        }
                         
                        
                    }
                    
                    /* NEW III - INDEPENDANT of GROUP INDIVIDUAL HIDE 
                    if(self.allSprites[currentSprite].className.includes('hideMeAfter') && (self.allSprites[currentSprite].className.lastIndexOf("groups") !=1)){

                        var pos = self.allSprites[currentSprite].className.lastIndexOf("groups")+6;
                        var res = self.allSprites[currentSprite].className.substr(pos, 1);                        

                        if(res != self.groupIndex){
                            self.allSprites[currentSprite].style.opacity = 0;
                        }
                        
                    } else {
                        self.allSprites[currentSprite].style.opacity = 1;
                    }
                    */
                    
                    
                    
                    self.allSprites[currentSprite].style.opacity = 1;
                    self.allSprites[currentSprite].classList.remove("fadeIn");

                }

            }

        }

    },

    /**
     * Format the time from seconds to M:SS.
     * @param  {Number} secs Seconds to format.
     * @return {String}      Formatted time.
     */
    formatTime: function (secs) {
        var minutes = Math.floor(secs / 60) || 0;
        var seconds = (secs - minutes * 60) || 0;

        return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
    },

    /**
        Find location of playhead 
    */
    getTheOffset: function (el) {
        var rect = el.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return {
            top: rect.top + scrollTop,
            left: rect.left + scrollLeft,
            right: rect.right + scrollLeft,
            bottom: rect.bottom + scrollTop,
            width: rect.width,
            height: rect.height
        }
    },

    /**
     * Returns the percentage of the click on the timeline bar 
     */
    clickPercent: function (el) {

        var self = this;

        /*
        consoleMeAudioPlayer(">>> clickPercent >");
        consoleMeAudioPlayer((el.pageX - self.getTheOffset(timeline).left) / self.getTheOffset(timeline).width);
        */

        var getPercent = (el.pageX - self.getTheOffset(timeline).left) / self.getTheOffset(timeline).width;

        // Go over the borders
        if (getPercent < 0) {
            getPercent = 0;
        }
        if (getPercent > 1) {
            getPercent = 1;
        }

        return getPercent;
    },

    /**
        Make the Timeline Clickable
    */
    clickTimeline: function (e) {

        var self = this;

        self.moveplayhead(e);
        self.doSeek(self.clickPercent(e));
    },

    /**
        On Playhead MouseDown
    */
    mouseDown: function (e) {

        consoleMeAudioPlayer(">>> mouseDown >")

        var self = this;

        // Mouse is Down
        self.onplayheadMouseDown = true;

        // Disactivate iFrame Events (so MouseMove is active)
        $('#activityIFrame').css('pointer-events', 'none');

        // Pause the audio
        self.pause();

        // Assign MouseMove on the top Window               
        self.theParentWindow.addEventListener('mousemove', this.bindMeWithThis);
    },

    /**
        On Window MouseUp
    */
    mouseUp: function (e) {

        consoleMeAudioPlayer(">>> mouseUp >")

        var self = this;


        if (self.onplayheadMouseDown == true) {

            self.moveplayhead(e);

            self.doSeek(self.clickPercent(e));

            // Remove MouseMove on the top Window
            self.theParentWindow.removeEventListener('mousemove', this.bindMeWithThis);

            // Activate iFrame Events Again
            $('#activityIFrame').css('pointer-events', 'auto');

            // Mouse is not Down
            self.onplayheadMouseDown = false;
        }

    },

    /**
        On Window MouseUp
    */
    moveplayhead: function (e) {

        var self = this;

        var playHeadLeft = self.getTheOffset(playhead).left;
        var timelineLeft = self.getTheOffset(timeline).left;
        var timelineRealWidth = timeline.offsetWidth - playhead.offsetWidth;
        //var timelineRealWidth = self.getTheOffset(timeline).width;
        var newMargLeft = e.pageX - timelineLeft;

        if (newMargLeft >= 0 && newMargLeft <= timelineRealWidth) {
            //playhead.style.marginLeft = newMargLeft + "px";
        }
        if (newMargLeft < 0) {
            newMargLeft = 0;
            //playhead.style.marginLeft = "0px";
        }
        if (newMargLeft > timelineRealWidth) {
            newMargLeft = timelineRealWidth;
            //playhead.style.marginLeft = timelineRealWidth + "px";
        }

        playhead.style.marginLeft = newMargLeft + "px";

    }

}; //end of prototype



// Globals
var player;
var calledOnce = false;
var myCurrentLinkID;

function loadAudioPlayer(exerciseRef, allSprites, audiofilename, syncAudio_Opacity, resetFromAudioPlayer) {

    if (!calledOnce) {
        // Initialize Global Object for the 1st Time & Load Current Activity Data
        player = new audioPlayer(exerciseRef);
        player.events_1StTime();

        /* Locate Audio By Activity Number
        player.audioFilename = acc.RESOURCES_FOLDER+'/audio/'+acc.state;
        */
        // Specify the current Audio File:
        //player.audioFilename = audiofilename;

        //Load Audio + Player Functions
        //player.initCurrentPlayer(allSprites);
        
        calledOnce = true;

    } // End IF

    if (audiofilename) {


        // reset navigation to delay the audio load
        document.getElementById("previous").disabled = true;
        document.getElementById("next").disabled = true;

        //player.dataJSON = undefined;
        //delete player;

        // Load Current Activity Data
        player.exerciseRef = exerciseRef;
        player.dataJSON = exerciseRef.activity.timelineObject;

        /* Locate Audio By Activity Number
        player.audioFilename = acc.RESOURCES_FOLDER+'/audio/'+acc.state;
        */
        // Specify the current Audio File:
        player.audioFilename = audiofilename;

        player.initCurrentPlayer(allSprites);

    } else {

        // Specify the current Audio File:
        player.audioFilename = audiofilename;

        //Load Audio + Player Functions
        player.initCurrentPlayer(allSprites);

        // Disactivate Events in the Timeline
        player.totalDisactivate();

        // Shows Possible Extra Activity
        if (xtraActivityExists) {
            extraActivityShow();
        }

        updateButtons(myCurrentLinkID);

    } // End 2nd IF

    
    if (syncAudio_Opacity!=undefined) {
        player.myOpacity = syncAudio_Opacity;
    }   

    if(resetFromAudioPlayer!=undefined) {
        player.resetFromAudioPlayer = resetFromAudioPlayer;
    } else {
        player.resetFromAudioPlayer = function(){};
    }

}
