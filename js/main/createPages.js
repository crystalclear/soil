function createPages(linkID, myPosition, myLevel){
	var theHTML = "";
	if (Array.isArray(myPosition["level" + myLevel])) {
		theHTML += '<div id="levelPages">';
		for (i = 0; i < Object.size(myPosition["level" + myLevel]); i++) {
			if (myPosition["level" + myLevel][i].image) {
				theHTML += ' <div class="ImThumb"><div><p>' + localizeText(myPosition["level" + myLevel][i].title, acc.myLang) + '</p></div><img src="' + myPosition["level" + myLevel][i].image + '" alt="' + localizeText(myPosition["level" + myLevel][i].title, acc.myLang) + '" height=50% data-id="' + 'page' + linkID + '_' + i + '" class="ImThumb"/></div> ';
			}
		}
		theHTML += '</div>';
	}
	else {
		if (myPosition["level" + myLevel].image) {
			theHTML += ' <img src="' + myPosition["level" + myLevel].image + '" alt="' + localizeText(myPosition["level" + myLevel].title, acc.myLang) + '" /> ';
		}
	}
	document.querySelector("#selides").className = "";
	$("#selides").html(theHTML);
	document.querySelector("#levelPages").style.paddingTop = 0;
	document.querySelector("#levelPages").style.paddingLeft = 0;
	document.querySelector("#levelPages").style.width = "auto";
	var allImThumbs = document.querySelectorAll("img.ImThumb");
	while(document.querySelector("#levelPages").scrollHeight>=550-25){
		if (allImThumbs.length > 0)
		{
			for (i = 0; i < allImThumbs.length; i++)
			{
				allImThumbs[i].style.height = (allImThumbs[i].scrollHeight-5)+"px";
			}
		}
		
	}
	document.querySelector("#levelPages").style.paddingTop = ((((550-25)-document.querySelector("#levelPages").scrollHeight)/2)+25)+"px";
	//alertMe(document.querySelector("#levelPages").scrollWidth+"/"+$("#levelPages").innerWidth()+"/"+$("#levelPages").width());
	document.querySelector("#levelPages").style.paddingLeft = ((((1024-14)-(document.querySelector("#levelPages").scrollWidth-40))/2))+"px";
	document.querySelector("#levelPages").style.width = "990px";
	if (allImThumbs.length > 0)
	{
		for (i = 0; i < allImThumbs.length; i++)
		{
			allImThumbs[i].addEventListener("click",
				function () {
					clickImage(this);
				});
		}
	}
}

// LOAD PAGE CONTENT *************************************
var theCurrentPageName;

function openSubsVideo() {
    if (document.querySelectorAll("#pageElementSubs > img")[0].src.indexOf("nosubtitles") === -1) {
        document.querySelectorAll("#pageElementSubs > img")[0].src = acc.RESOURCES_FOLDER+"/layout/menu/nosubtitles.png";
        // LOAD VIDEO SRC
        document.querySelectorAll("#thePageElementsContent > video > source")[0].src = acc.RESOURCES_FOLDER+"/video/" + theCurrentPageName + "_subs.mp4";
        document.querySelectorAll("#thePageElementsContent > video > source")[1].src = acc.RESOURCES_FOLDER+"/video/" + theCurrentPageName + "_subs.ogg";
        //alertMe("SUBS")
    } else {
        document.querySelectorAll("#pageElementSubs > img")[0].src = acc.RESOURCES_FOLDER+"/layout/menu/subtitles.png";
        // LOAD VIDEO SRC
        document.querySelectorAll("#thePageElementsContent > video > source")[0].src = acc.RESOURCES_FOLDER+"/video/" + theCurrentPageName + ".mp4";
        document.querySelectorAll("#thePageElementsContent > video > source")[1].src = acc.RESOURCES_FOLDER+"/video/" + theCurrentPageName + ".ogg";
        //alertMe("NO SUBS")
    }

    var videoTag = $('#thePageElementsContent video')[0];
    videoTag.load();
    videoTag.play();
}


/* DISACTIVATED

function closeVideo() {
    $('#thePageElementsDiv').css('visibility', 'hidden');
	
	var videoTag = $('#thePageElementsContent video')[0];
	videoTag.load();
	videoTag.pause();
	$('#video').html('');
	$('.captions-menu').remove();
}


function loadVideo(linkID) {

    var extraArrayInfo = linkID.split("@");
    //alertMe("LOAD VIDEO: "+extraArrayInfo);

    if (extraArrayInfo[1] === "audio") {
        var audioPath = acc.RESOURCES_FOLDER+"/audio/" + extraArrayInfo[2] + ".";
        initAudioPlayer(audioPath);
        handleAudio("audiopl");
        document.getElementById("audiopl").disabled = false;

    } else if (extraArrayInfo[1] === "video") {
        // OPEN VIDEO SCREEN BACKGROUND
        $('#thePageElementsDiv').css('visibility', 'visible');

        // Keep Global Filename
        theCurrentPageName = extraArrayInfo[2];

        // ASIIGNE EVENTS for close and subs load
        var theButtonExtra = document.getElementById("pageElementClose");
        //$('#pageElementClose').css('visibility', 'visible'); //if you force visibility, hiding the parent will not hide children (chrome)
        try {
            theButtonExtra.removeEventListener("click", closeVideo);
        } catch (err) {
        }
        theButtonExtra.addEventListener("click", closeVideo);

        var theButtonSubs = document.getElementById("pageElementSubs");
        try {
            theButtonSubs.removeEventListener("click", openSubsVideo);
        } catch (err) {
        }
        theButtonSubs.addEventListener('click', openSubsVideo);

       var videoTag = $('#thePageElementsContent video')[0];

        // LOAD VIDEO SRC
        document.querySelectorAll("#pageElementSubs > img")[0].src = acc.RESOURCES_FOLDER+"/layout/menu/subtitles.png";
		
		
		var myVideoName = acc.RESOURCES_FOLDER+"/video/" + extraArrayInfo[2];
		 
		 $('#video').html('<source src="'+myVideoName+'.mp4" type="video/mp4"><source src="'+myVideoName+'.ogg" type="video/webm"><track label="English" kind="captions" srclang="en" src="'+myVideoName+'.vtt" default>');
		
		// VIDEO CAPTIONS			
        // AUTOPLAY
		setTimeout(function(){runVideo(); videoTag.play();}, 1000);

    } else if (extraArrayInfo[1] === "activityVideo") {
        // OPEN VIDEO SCREEN BACKGROUND
        $('#thePageElementsDiv').css('visibility', 'visible');

        // Keep Global Filename
        theCurrentPageName = extraArrayInfo[2];

        // ASIIGNE EVENTS for close and subs load
        var theButtonExtra = document.getElementById("pageElementClose");
        $('#pageElementClose').css('visibility', 'hidden');
        try {
            theButtonExtra.removeEventListener("click", closeVideo);
        } catch (err) {
        }

        var videoTag = $('#thePageElementsContent video')[0];

        var cutExtraArrayInfo = extraArrayInfo[2].split("_");
		
		var myVideoName2 = acc.ACTIVITIES_FOLDER + "/" + cutExtraArrayInfo[0] + "/" + cutExtraArrayInfo[1] + "/" + cutExtraArrayInfo[2] + "/video";
		
		$('#video').html('<source src="'+myVideoName2+'.mp4" type="video/mp4"><source src="'+myVideoName2+'.ogg" type="video/webm"><track label="English" kind="captions" srclang="en" src="'+myVideoName2+'.vtt" default>');
       // VIDEO CAPTIONS			
        // AUTOPLAY
		setTimeout(function(){runVideo(); videoTag.play();}, 1000);
    }
}
*/

function loadExercise(linkID)
{

    /* Stop the AudioPlayer */
    if(player){
        player.stop();
    };
    
    
    
    
    var myPath, i;
    var cleanID = linkID + "";
    if (linkID[0] == "k") {
        cleanID = linkID.substr(7);
    }
    acc.linkID = cleanID + "";
    var linkElem = cleanID.split("_");
    myPath = acc.ACTIVITIES_FOLDER + "/";
    if (linkElem.length > 1) {
        for (i = 0; i < linkElem.length - 1; i++) {
            myPath = myPath + linkElem[i] + "/";
        }
    }

    // AUDIO PLAYER
    var audioPath = acc.RESOURCES_FOLDER+"/audio/";
    for (a = 0; a < linkElem.length; a++) {
        if (a < (linkElem.length - 1)) {
            audioPath += linkElem[a] + "_";
        } else {
            audioPath += linkElem[a];
        }
    }
    //initAudioPlayer(audioPath + ".");
    // AUDIO PLAYER
    // AUDIO SCRIPT

    // ACTIVITY + OBJECT LOAD (path+data)	
	if (acc.linkID == "home"){
		myPath = "activities/activity.html";
	}
    else if (acc.linkID == "end"){
        myPath = "activities/activityEnd.html";
    }
	else{		
		acc.currentActFolder = myPath + linkElem[linkElem.length - 1]+"/";

		loadActJSON(acc.currentActFolder+"data/activity.json", function(response) {
			// Parse JSON string into object
			//consoleMe(JSON.stringify(response));
			activity = JSON.parse(response);
			var myPath = "";
			var localTypeOfActivity = activity;
			localTypeOfActivity=activity["ex"][0]["type"];
			consoleMe('localTypeOfActivity '+localTypeOfActivity);
            
			switch (localTypeOfActivity) {
            case "display":
				myPath = "html/display.html"; 
				break;
            case "multiple":
				myPath = "html/multiple.html"; 
				break;
			case "dnd":
                myPath = "html/dnd.html"; 
				break;
			case "match":
                myPath = "html/match.html"; 
				break;
			case "typein":
                myPath = "html/typein.html"; 
				break;
			case "underline":
                myPath = "html/underline.html"; 
				break;	
			case "clickReveal":
                myPath = "html/clickReveal.html"; 
				break;
			case "syncAudio":
                myPath = "html/syncAudio.html"; 
				break;
			case "syncClick":
				myPath = "html/syncClick.html"; 
				break;
            case "custom":
                myPath = acc.currentActFolder+"index.html"; 
				break;
            default :
				myPath = "html/activity.html"; 
			}
            /* NEW UNUSED
			 if (!hasClass(document.querySelector("#selida"), "hideMe")) {
				document.querySelector("#selida").className = document.querySelector("#selida").className + " hideMe";
			}
			if (!hasClass(document.querySelector("#selidaLinks"), "hideMe")) {
				document.querySelector("#selidaLinks").className = document.querySelector("#selidaLinks").className + " hideMe";
			}
			if (!hasClass(document.querySelector("#selides"), "hideMe")) {
				document.querySelector("#selides").className = document.querySelector("#selides").className + " hideMe";
			}
            */
			document.querySelector("#myObject").innerHTML = '<iframe id="activityIFrame" name="activityIFrame" allowfullscreen src="' + myPath + '" ><\/iframe>';
			consoleMe('myPath.innerHTML '+myPath)
			// ON ACTIVITY LOAD - CHANGE BUTTONS !!!!!!!!!!!!!

			var myActObj = document.getElementById('activityIFrame');

			myActObj.onload = function () {
				var myObbH;
				//Calculate OBJECT height
				/*
                try {
					myObbH = $(document.getElementById("activityIFrame").contentWindow.document.body.childNodes[3]).height();
				} catch (err) {
					myObbH = $(document.getElementById("activityIFrame").contentDocument.body.childNodes[3]).height();
				}
				document.getElementById("activityIFrame").setAttribute("height", myObbH);
                */
				document.getElementById("content_wrapper").style.overflow = "auto";
				/*14/07/2015*/
				/*Commented following 2 lines just to avoid error on local version*/
				//resetExtraTextButtons();
				//setExtraTextButtons();
				enableToolbar();
				myActObj.onload = null;
			};
			acc.state = acc.linkID;
			acc.unitState.currentPosition = linkElem[linkElem.length-1];
			
		});
		
	}
   
}

