/*****
 * Disable it
 *****/

/*****
 * Allow to reposition the main window to the left
 *****/

var jBox__CCS_repositionMain = false;


// Path for Book Graphics
var bookResourcesPath = acc.RESOURCES_FOLDER + '/layout/mainXtraButs/'

var theBookBut = document.getElementById('bookButton');
theBookBut.disabled = true;
theBookBut.querySelector('img').src = bookResourcesPath + 'book_disabled.png';

/*
var theAttachmentBut = document.getElementById('attachmentButton');
theAttachmentBut.disabled = true;
theAttachmentBut.querySelector('img').src = bookResourcesPath + 'attachment_disabled.png';
*/

/* Get Position of Elements */
function getTheBoundaries(div) {

    var rect = div.getBoundingClientRect();
    l = rect.left;
    r = rect.right;
    t = rect.top;
    b = rect.bottom;
    x = rect.x;
    y = rect.y;
    w = rect.width;
    h = rect.height;

    return {
        l: l,
        r: r,
        t: t,
        b: b,
        x: x,
        y: y,
        w: w,
        h: h
    };
}

var popMaxWidth = 300;
var popMaxHeight = 500;

var myTooltip = new jBox('Tooltip', {});

function createjBox(theContent) {

    var divContent = document.querySelector('.theContent');
    var popContentBounds = getTheBoundaries(divContent);
    //console.log("popContentBounds> l: " + l + " r: " + r + " t: " + t + " b: " + b + " x: " + x + " y: " + y + " w: " + w + " h: " + h);

    var divNavigation = document.querySelector('.theNavigation');
    var popNavigationBounds = getTheBoundaries(divNavigation);
    //console.log("popNavigationBounds> l: " + l + " r: " + r + " t: " + t + " b: " + b + " x: " + x + " y: " + y + " w: " + w + " h: " + h);

    var divBookButton = document.querySelector('#bookButton');
    var popBookButtonBounds = getTheBoundaries(divBookButton);
    //console.log("popBookButtonBounds > l: " + l + " r: " + r + " t: " + t + " b: " + b + " x: " + x + " y: " + y + " w: " + w + " h: " + h);
    
    myTooltip = new jBox('Tooltip', {
        attach: '#bookButton',
        target: '#nav-wrapper',
        trigger: 'click',
        /*
        delayOpen: 500,
        delayClose: 200,
        */
        draggable: true,

        width: popMaxWidth,
        height: popContentBounds.h-30,


        /*
        minWidth: popMaxWidth,
        minHeight: popMaxHeight,

        maxWidth: popMaxHeight,
        maxHeight: popMaxHeight,
        */
        


        /*** 
        * Only if Position is NOT set 
        
        adjustPosition: true,
        adjustTracker: true,
        pointer: 'right',
        ***/



        /*
        attributes: {
          x: 'right',
          y: 'bottom'
        },
        */
        
        outside: 'xy', // With the option outside you can move your jBox outside of the targets position, use x, y or xy. | Horizontal Tooltips need to change their outside position
        
        offset: {
            /*
            x: popBookButtonBounds.r/2+popMaxWidth/2,
            x: (popContentBounds.r-popBookButtonBounds.r-popMaxWidth/2+popBookButtonBounds.w),
            */
            
            /*
            x: (popContentBounds.r-popBookButtonBounds.r),
            y: -popNavigationBounds.h/2
            */
            x: -popMaxWidth+popBookButtonBounds.w
        },

        /* When Activated Pointer & Other options DO NOT work */
        position: {
            /*
            x: popContentBounds.r - popMaxWidth,
            y: popNavigationBounds.h
           */

            /*
            x: popContentBounds.r - popMaxWidth,
            y: popContentBounds.b - popMaxHeight - popNavigationBounds.h/2 + 5
            */
            
            x: 'right'

        },
        
        /*
        reposition: true,
        repositionOnOpen: true,
        repositionOnContent: true,
        responsivePositions: null,
        */

        /*
        content: 'blabla bla bla blalllb bbla blabla',
        */
        title: '<div class="containerCloseJBox"><div id="closeJBox">x</div></div>',
        onCreated: function () {
            document.querySelector('#closeJBox').addEventListener("click", function () {
                myTooltip.close();
                
                // Maximize Main Again
                reMaximizeMain();
            });
        },
        onDragStart: function () {
            // Maximize Main Again
            reMaximizeMain();
        },
        onPosition: function () {
            //alert("POSITIONED")
        },
        animation: 'slide:right'

    });
    
    //myTooltip.setTitle('Hello!').setContent('Some content...');
    myTooltip.setContent(theContent);
    
}

function reMaximizeMain() {
    const contentContainer = document.getElementsByClassName("theContent")[0];
    contentContainer.style.flex = '0 0 100%'
}

function loadtheBook(theContent) {
    theBookBut.disabled = false;
    theBookBut.querySelector('img').src = bookResourcesPath + 'book.png';
    //myTooltip.setContent(theContent);
    
    myTooltip.destroy();    
    createjBox(theContent);
    
    // Maximize Main Again
    reMaximizeMain();
}

function unLoadtheBook() {
    theBookBut.disabled = true;
    theBookBut.querySelector('img').src = bookResourcesPath + 'book_disabled.png';
    //myTooltip.close();
    
    myTooltip.destroy();
    
    // Maximize Main Again
    reMaximizeMain();
}



