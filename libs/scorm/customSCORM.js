//javascript
var scorm = pipwerks.SCORM;
var lmsConnected = false;
var unloaded = false;
var hasSCORMscore = false;

function handleError(msg){
   alertMe("handleError"+ msg);
//   window.close();
}

function initCourse(){
 
   //scorm.init returns a boolean
   lmsConnected = scorm.init();
   //If the scorm.init function succeeded...
   if(lmsConnected){
      //Let's get the completion status to see if the course has already been completed
      var completionstatus = scorm.get("cmi.core.lesson_status");
		//consoleMe(completionstatus)
      //If the course has already been completed...
      if(completionstatus == "completed" || completionstatus == "passed"){
 
         //...let's display a message and close the browser window
         //handleError("You have already completed this course. You do not need to continue.");
 
		}
      //Now let's get the username from the LMS
      var learnername = scorm.get("cmi.core.student_name");
	
      //If the name was successfully retrieved...
      if(learnername){  
 
		//consoleMe("Welcome back " + learnername);
         //...let's display the username in a page element named "learnername"
         //document.getElementById("learnername").innerHTML = learnername; //use the name in the form
 
      }
 
   //If the course couldn't connect to the LMS for some reason...
   } else {
 
      //... let's consoleMe the user then close the window.
      //handleError("Error: Course could not connect with the LMS");
 
   }
 
}

function setComplete(){
 
   //If the lmsConnection is active...
   if(lmsConnected){
 
      //... try setting the course status to "completed"
      var success = scorm.set("cmi.core.lesson_status", "completed");
 
      //If the course was successfully set to "completed"...
      if(success){
 
         //... disconnect from the LMS, we don't need to do anything else.
         //scorm.quit();
 
      //If the course couldn't be set to completed for some reason...
      } else {
 
         //consoleMe the user and close the course window
         //handleError("Error: Course could not be set to complete!");
		 scorm.set("cmi.completion_status", "completed");
		 scorm.save();
		 //scorm.quit();
      }
 
   //If the course isn't connected to the LMS for some reason...
   } else {
 
      //consoleMe the user and close the course window
      //handleError("Error: Course is not connected to the LMS");
 
   }
 
}

function initForm(){
 
  document.getElementById("myform").onsubmit = function (){
 
    this.innerHTML = "Thank you, your selection has been recorded. You may close this window.";
 
    setComplete();
 
    return false; // This prevents the browser from trying to post the form results
 
  }
 
}

function unloadHandler(){
   if(!unloaded){
      scorm.save(); //save all data that has already been sent
      scorm.quit(); //close the SCORM API connection properly
      unloaded = true;
   }
}

//use the SCORM wrapper function to set the score.

/*	This score may be determined and calculated in any manner that makes sense to the SCO designer. 
	For instance, it could reflect the percentage of objectives complete, it could be the raw score 
	on a multiple choice test, or it could indicate the number of correct first responses to embedded 
	questions in a SCO.
	
	--source: http://www.scormcourse.com/scorm12_course/resources/coreScoreRaw.html
 */
 
function setScoreResult(result, qcount, isScorable) {
	scorm.set("cmi.core.score.raw", result);
	scorm.set("cmi.core.score.max", qcount);
	scorm.set("cmi.core.score.min", qcount/2.5);
	
	  //scorm.set("cmi.core.lesson_status", "completed")
	  //completionstatus = scorm.get("cmi.core.lesson_status");
	  //consoleMe(completionstatus)
	
	if (isScorable) {	
//		scorm.set("cmi.core.score.raw", result);
		if (result > qcount/2.5) {
			scorm.set("cmi.core.lesson_status", "passed")
			//scorm.set("cmi.success_status", "passed");
			//consoleMe(qcount+" "+result)
		} else {
			scorm.set("cmi.core.lesson_status", "failed")
			//scorm.set("cmi.success_status", "failed");			
		}
	} else {
			scorm.set("cmi.core.lesson_status", "completed")
			//scorm.set("cmi.completion_status", "completed");
	}
	scorm.save();
	 //scorm.quit();
}

function determineSCORMcourseStatus() {
	//commiting to LMS
	setComplete();
}


$(document).ready(function() {
	window.onbeforeunload = unloadHandler;
	window.onunload = unloadHandler;
	initCourse();
});