function runIntroJs() {
    
    var introJsSteps = [
        {
          element: '#burgerButton',
          intro: "Click here to display or hide the navigation menu.",
          position: 'right'
        },
        {
            //element: document.getElementById("0_0"),
            element: document.querySelectorAll('.unitMenu')[0].querySelector('span'),
            intro: "Click on any menu item to see expanded contents or to navigate to a particular section.",
            position: 'right'
        },
        {
            element: document.querySelector('#btnXtraQuestionBut'),
            intro: "When you see this symbol, click to check your knowledge.",
            position: 'bottom-right-aligned'
        },        
        {
            element: document.querySelector('#goToEnd'),
            intro: "Click here to jump to the end of a slide.",
            position: 'bottom-right-aligned'
        },
        {
            element: document.querySelector('#bookButton'),
            intro: "Click here to view a transcript of the narration.",
            position: 'bottom-right-aligned'
        },
        {
            element: 'none',
            intro: "Optimized for viewing on 1366X768 resolution or higher and 100% zoom.",
            position: 'center'
        }
    ];
    

    
    
    
    /* initialize an introjs instance          
    var intro = introJs();
    // load data
    intro.setOptions({steps: Steps });
    // start intro.js
    intro.start();
    */
    
    /*
    .setOptions({steps: introJsSteps })
    .setOption("prevLabel", "back")
    .setOption("nextLabel", "next")
    .setOption('showProgress', true)
    .setOption('showBullets', false)
    .setOption('showStepNumbers', false)
    .setOption('overlayOpacity', 0.4)
    .setOption('highlightClass', "highlightClass")
    
    */
    
    
    introJs().setOptions({steps: introJsSteps, showProgress:true, prevLabel: "Back", nextLabel: "Next", skipLabel: "<div class='closeIntro'>x</div>", doneLabel: "Close", showBullets: false, showStepNumbers: false, overlayOpacity: 0.1, highlightClass: "highlightClass", disableInteraction:true}).onexit(function() {
        /*$("nav").css("opacity", 1);*/
        document.querySelector('nav').style.display = 'none';
        document.querySelector('.extraQuestion').style.opacity = 1;
        $('#btnXtraQuestionBut').hide();
    }).onchange(function(targetElement) {  
        /*
        if(this._currentStep==3){
            $('#btnXtraQuestionBut').hide();
        }
        */
    }).start(); 
    

    
    /*
    .oncomplete(function() {
        document.querySelector('nav').style.display = 'none';
        alert("end of introduction");
    })
    */
    
}