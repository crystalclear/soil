// Path for Extra Activity Buttons
var xtraActivityResourcesPath = acc.RESOURCES_FOLDER + '/layout/activities/'

// Global Param to indicate the Xtra Activity Exists
var xtraActivityExists = false;
// Global Param to indicate the Xtra Activity as Been Open Already
var xtraActivityWasOpen = false;

function disableElem_ExtraActivity(){
    // Disable popOver + Button #btnXtraQuestionBut
    $('#btnXtraQuestionBut').popover('disable');
    $('#btnXtraQuestionBut').prop('disabled', true);

    // Hide the button
    $('#btnXtraQuestionBut').hide();
}
function resetHide_ExtraActivity(){
    // On new Activity that has an Extra Activity, clean the previousPopover / Extra Activity
    $('#btnXtraQuestionBut').popover("dispose");   
    
    // Hide All Extra Activity + Including the button
    $('#btnXtraQuestionBut').popover('hide');
    $('#btnXtraQuestionBut').prop('disabled', false);
    $('#btnXtraQuestionBut').hide();
}
// Function to clean Events of popOver after Hiding Animation Ends
function cleanOnEndPopOver(theContent, myAnswer) {
    
    // Remove Hiding CSS Class & Transition class for not delaying the popOver hide
    //$(".popover").removeClass("popOverTransition slideMeOutLeft");
    $(".popover").removeClass("popOverTransition slideMeOutLeft");
    // Empty Extra Activity Content
    $(".popover-header").html("");
    
    // *** Add New Content *****
    if(myAnswer){
        // *** Add New Correct Content *****
        $(".popover-body").html(theContent.feedbackCorrect+'<div><button id="btnExtraQuestionClose">OK</button></div>');
        $(".popover-body").css("color", "green");

    } else {
        // *** Add New Wrong Content *****
        $(".popover-body").html(theContent.feedbackWrong+'<div><button id="btnExtraQuestionClose">OK</button></div>');
        $(".popover-body").css("color", "red");
    }   
   
    // Open Animated Feedback
    $(".popover").addClass("popOverShowFeedBackTransition slideMeInLeft");
    
    //Assign the OK button to close the popOver
    $("#btnExtraQuestionClose").click(function() {
        
        // Reset Classes for popOver
        $(".popover").removeClass("popOverTransition slideMeInLeft slideMeOutLeft");
        
        // remove Click
        $("#btnExtraQuestionClose").off('click');
        
        // Hide All Extra Activity + Including the button
        resetHide_ExtraActivity();        
        
     });
                            
    // Actually hide the popOver
    //$('#btnXtraQuestionBut').popover('hide');
    
    // Remove current Listener: 'transitionend'
    this.removeEventListener('transitionend', cleanOnEndPopOver);

    console.log('transitionend fired'); 
}


/* localCopyOfSettings.elements.facilitator */
function loadExtraActivity(theContent) {
    
    // Global Param to indicate the Xtra Activity Exists
    xtraActivityExists = true;
    // Global Param to indicate the Xtra Activity as Been Open Already
    xtraActivityWasOpen = false;
    
    // Hide All Extra Activity + Including the button
    resetHide_ExtraActivity();  
     
    // Show the button that opens the popOver / Extra Activity
    //$('#btnXtraQuestionBut').show(1000);
    
    // ** Load the popOver Content and options **//
    //$('#btnXtraQuestionBut').popover({title: theContent.stem, content: '<span class="xtraAnswers"><img src="'+xtraActivityResourcesPath+'radio_btn_idle.png"></span>'+theContent.answer1+'<br><span class="xtraAnswers"><img src="'+xtraActivityResourcesPath+'/radio_btn_idle.png"></span>'+theContent.answer2, html: true, placement: "right", trigger : 'click', animation: true, delay:{'show':'10', 'hide':'10'}});
    /*
    $('#btnXtraQuestionBut').popover({title: theContent.stem, content: '<div class="xtraAnswersContainer"><span class="xtraAnswersButtons"><img src="'+xtraActivityResourcesPath+'xtraRadio.png"></span><span class="xtraAnswers">'+theContent.answer1+'</span><span class="xtraAnswersButtons"><img src="'+xtraActivityResourcesPath+'xtraRadio.png"></span><span class="xtraAnswers">'+theContent.answer2+'</span></div>', html: true, placement: "right", animation: true});
    */
    $('#btnXtraQuestionBut').popover({title: theContent.stem, content: '<div class="xtraAnswersContainer"><span class="xtraAnswers">'+theContent.answer1+'</span><span class="xtraAnswers">'+theContent.answer2+'</span></div>', html: true, placement: "right", animation: true});
   
    
    
    // Assign Buttons Events, after showing the extra Activity
    $("#btnXtraQuestionBut").on('shown.bs.popover', function(){
        
        /* Get popOver Dynamic Element*/
        var thePopOver = document.querySelector('.popover');

        // Reset Classes for popOver
        $(".popover").removeClass("popOverTransition slideMeInLeft slideMeOutLeft");
        
        
        // Assign Answers the Listeners
        $(".xtraAnswers").click(function() {
            
            // Delete Init /1st Step Animation Classes
            $(".popover").removeClass("initPopOverTransition initSlideMeInLeft initSecondHidinPopOverTransition slideMeOutLeft");
            
            // Show the options
            //$(this).find('img').attr('src', xtraActivityResourcesPath+'radio_btn_pressed.png');
            
            if($(".xtraAnswers").index(this) == theContent.correct){
                //$(this).find('img').attr('src', xtraActivityResourcesPath+'radio_btn_correct.png');
                $(this).find('img').attr('src', xtraActivityResourcesPath+'corr.png');
                $(this).css('color', 'white');
                //$(this).css('borderColor', 'white');
                $(this).css('background-color', 'green');
                var myAnswer = true;
            } else {
                //$(this).find('img').attr('src', xtraActivityResourcesPath+'radio_btn_wrong.png');
                $(this).find('img').attr('src', xtraActivityResourcesPath+'wrong.png');
                $(this).css('color', 'white');
                //$(this).css('borderColor', 'white');
                $(this).css('background-color', 'red');
                myAnswer = false;
                
            }            
            
            // remove Both Clicks
            $(".xtraAnswers").off('click');
                       
            // Add the class to hide it with animation
            $(".popover").addClass("popOverTransition slideMeOutLeft");
            
            // Disable popOver + Button #btnXtraQuestionBut
            disableElem_ExtraActivity();
            
            // Add an event to catch the end of Animation to load new content / feedback
            thePopOver.addEventListener('transitionend', function(){
                cleanOnEndPopOver(theContent, myAnswer);
            }, false);
            
            
            
            /* Alternative Solution with jQuery Delay  
            $(".popover").delay(1100).queue(function() {
                
                
                $('#btnXtraQuestionBut').popover('hide');
                $(".popover").removeClass("popOverTransition slideMeOutLeft");
                
                
                $(this).dequeue();

            }); 
            */

            //alert(".xtraAnswers.click event > hide me with animation");
        }); 
        
        console.log("!!! shownnnnnnnnnn")
    });
    
    /* Other popOver Events
    $("#btnXtraQuestionBut").on('show.bs.popover', function(){
        console.log("show")
    });
    $("#btnXtraQuestionBut").on('hide.bs.popover', function(){
        console.log('The popover is about to be hidden.');
    });
     $("#btnXtraQuestionBut").on('hidden.bs.popover', function(){
        
        console.log('The popover is now hidden.');
    });
    
   */
    
    
    //alert("loadExtraActivity");
}

function unLoadExtraActivity() {
    
    // Change the Global Param to false
    xtraActivityExists = false;
    // Global Param to indicate the Xtra Activity as Been Open Already
    xtraActivityWasOpen = false;
    
    $('#btnXtraQuestionBut').hide(500);
    $('#btnXtraQuestionBut').popover("dispose");
    
    //alert("unLoadExtraActivity")
}



function extraActivityShow() {
    // Show the button that opens the popOver / Extra Activity
    if(!xtraActivityWasOpen){
       $('#btnXtraQuestionBut').show(500);
    }
    // Global Param to indicate the Xtra Activity as Been Open Already
    xtraActivityWasOpen = true;
    
    console.log("extraActivityShow")
}


/* All popOvers work 
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();   
});
*/


/* INIT - Hide the button */
//$('#btnXtraQuestionBut').hide();

