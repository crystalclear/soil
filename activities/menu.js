﻿var txtMenu = {"menu":
{
    "title":'Resilient and healthy soil​',
	"level1": [

        {"title":"CULTIVATION AND SCOPE​", "x":"0", "y":"0", "type":"exercise"},
        {"title":"PRODUCTS PER SCOPE​", "x":"0", "y":"0", "type":"exercise"},
        {"title":"PRODUCER MODULE​​", "x":"0", "y":"0", "type":"exercise"},
        {"title":"NATURE MODULE​​", "x":"0", "y":"0", "type":"exercise"},
        {"title":"BIODIVERSITY​", "x":"0", "y":"0", "type":"exercise"},
        {"title":'SOIL', "image":"", 
            "level2":[
                {"title":"Resilient and healthy soil​​", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Minimum soil quality​​", "x":"0", "y":"0", "type":"exercise"},
				{"title":"Excluded plots​", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Compliance criteria​", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Example", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Activity 1​", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Activity 2​", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Activity 3​", "x":"0", "y":"0", "type":"exercise"},
                {"title":"Results​​", "x":"0", "y":"0", "type":"exercise"}
            ]
        }, 
        {"title":"ENERGY", "x":"0", "y":"0", "type":"exercise"},
        {"title":"OTHER MEASURES", "x":"0", "y":"0", "type":"exercise"}
    ]
}};